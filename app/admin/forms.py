# -*- coding: utf-8 -*-
from django.forms import ModelForm
from core.models import YesTicket


class DetailTicketForm(ModelForm):
     class Meta:
         model = YesTicket
         fields = '__all__'