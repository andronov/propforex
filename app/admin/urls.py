from django.conf.urls import patterns, url
import admin
from admin.views import login, UsersAdminEditView, ReferalOutView, ReferalOutEditView, notification, TicketAddView, \
    NotificationEditView, TicketDeleteView, UsersAdminDeleteView, OrderCallListView, PageListView, PageListEditView, PageListEditAddView
from django.core.urlresolvers import reverse, reverse_lazy
from admin.views import UsersAdminView, TicketListView, DetailTicketView, SettingsTicketView, SettingsTicketEditView, \
    SettingsTicketAddView, SettingsTicketDeleteView, CurencyEditView, OrderCallEditView


urlpatterns = patterns('',
    url(r'^users/delete/(?P<pk>[-\w]+)/$', UsersAdminDeleteView.as_view(success_url=reverse_lazy('admin-users')), name='admin-users-delete'),
    url(r'^users/(?P<pk>[-\w]+)/$', UsersAdminEditView.as_view(success_url=reverse_lazy('admin-users')), name='admin-users-edit'),
    url(r'^users', UsersAdminView.as_view(), name='admin-users'),
    url(r'^send/user/$', admin.views.send_user, name='send-user'),

    url(r'^tickets/settings/add', SettingsTicketAddView.as_view(), name='admin-tickets-settings-add'),
    url(r'^tickets/settings/delete/(?P<pk>[-\w]+)/$', SettingsTicketDeleteView.as_view(), name='admin-tickets-settings-delete'),
    url(r'^tickets/settings/(?P<pk>[-\w]+)/$', SettingsTicketEditView.as_view(success_url=reverse_lazy('admin-tickets-settings')), name='admin-tickets-settings-edit'),
    url(r'^tickets/settings', SettingsTicketView.as_view(), name='admin-tickets-settings'),
    url(r'^tickets/delete/(?P<pk>[-\w]+)/$', TicketDeleteView.as_view(), name='admin-tickets-delete'),
    url(r'^tickets/add', TicketAddView.as_view(), name='admin-tickets-add'),
    url(r'^tickets', TicketListView.as_view(), name='admin-tickets'),
    url(r'^ticket/(?P<pk>[-\w]+)/$', DetailTicketView.as_view(success_url=reverse_lazy('admin-tickets')), name='admin-tickets-edit'),

    url(r'^currency/', CurencyEditView.as_view(), name='currency'),
    url(r'^bids/(?P<pk>[-\w]+)/$', OrderCallEditView.as_view(), name='order-admin-edit'),
    url(r'^bids/', OrderCallListView.as_view(), name='admin-bids'),
    url(r'^notification/', NotificationEditView.as_view(), name='admin-notification'),
    url(r'^affilate/(?P<pk>[-\w]+)/$', ReferalOutEditView.as_view(), name='affilate-admin-edit'),
    url(r'^affilate/', ReferalOutView.as_view(), name='affilate-admin'),
    url(r'^api/notification/', notification, name='notification-api'),

    url(r'^pages/add', PageListEditAddView.as_view(), name='admin-pages-add'),
    url(r'^pages/(?P<pk>[-\w]+)/$', PageListEditView.as_view(), name='admin-pages-edit'),
    url(r'^pages', PageListView.as_view(), name='admin-pages'),

    url(r'^login/$', login, {}, name='auth_login_manager'),

)