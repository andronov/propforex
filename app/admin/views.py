# -*- coding: utf-8 -*-
import datetime
import json
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail, EmailMultiAlternatives
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect, resolve_url
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView, TemplateView
from accounts import UserProfile
from accounts.models import BidPayment, Notification, Referal, Curency
from admin.forms import DetailTicketForm
from arcticles.models import Article
from core.models import YesTicket, Ticket, OrderCall
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
            logout as auth_logout, get_user_model)
from django.utils.translation import ugettext_lazy as _

def send_user(request):
    if request.POST:
        data = {}
        users = UserProfile.objects.get(id=int(request.POST['user']))
        if users.send:
           ticket = YesTicket.objects.get(id=int(request.POST['ticket']))
           ticket.send = True
           ticket.save()
           text_content = 'Данные с доступом к счету активного билета'
           html_content = render_to_string('mailchimp/notification.html', {'users': users, 'ticket': ticket})
           msg = EmailMultiAlternatives('Данные с доступом к счету активного билета', text_content, settings.EMAIL_HOST_USER,  [users.email])
           msg.attach_alternative(html_content, "text/html")
           msg.send()
           data['ok'] = 'Спасибо Ваша заявка принята!'
        else:
           data['error'] = 'Пользователь отключил возможность получать уведомления!'
        return HttpResponse(json.dumps({'data':data}), content_type="application/json")

#Список Пользователей
class UsersAdminView(ListView):
    queryset = UserProfile.objects.filter(is_webmaster=False).order_by('-created')
    template_name = 'admin/admin-users.html'

    def get_queryset(self):
        qs = super(UsersAdminView, self).get_queryset()
        if 'q' in self.request.GET:
            search = self.request.GET.get('q')
            qs = qs.filter(Q(first_name__iexact=search) | Q(last_name__iexact=search)
                              | Q(phone__exact=search)| Q(email__iexact=search))
        return qs

    def get_context_data(self, **kwargs):
        context = super(UsersAdminView, self).get_context_data(**kwargs)
        date_ = datetime.datetime.now()
        users = UserProfile.objects.filter(is_webmaster=False,created__day=date_.day, created__month=date_.month, created__year=date_.year).count()
        context['users'] = users
        context['referals'] = Referal.objects.all()
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(UsersAdminView, self).dispatch(*args, **kwargs)

#Редактирование Пользователя
class UsersAdminEditView(UpdateView):
    model = UserProfile
    template_name = 'admin/admin-users-edit.html'
    fields = ['first_name', 'last_name', 'phone', 'email', 'is_staff', 'is_active', 'date_joined', 'sistemas', 'number_sistem']

    def get_initial(self):
        super(UsersAdminEditView, self).get_initial()
        times = str(self.model().date_joined.day) +'.'+str(self.model().date_joined.month)+'.'+str(self.model().date_joined.year)
        self.initial = {"date_joined":times}
        return self.initial

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(UsersAdminEditView, self).dispatch(*args, **kwargs)

#Удаление пользователя
class UsersAdminDeleteView(DeleteView):
    model = UserProfile
    template_name = 'admin/admin-сonfirm-delete.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(UsersAdminDeleteView, self).dispatch(*args, **kwargs)

#Список билетов
class TicketListView(ListView):
    queryset = YesTicket.objects.all().order_by('-created')
    template_name = 'admin/admin-tickets.html'

    def get_context_data(self, **kwargs):
        context = super(TicketListView, self).get_context_data(**kwargs)
        if 'user' in self.request.GET:
            context['users'] = UserProfile.objects.get(id=int(self.request.GET.get('user')))
        return context

    def get_queryset(self):
        qs = super(TicketListView, self).get_queryset()
        if 'q' in self.request.GET:
            search = self.request.GET.get('q')
            qs = qs.filter(Q(user__first_name__iexact=search) | Q(user__last_name__iexact=search)
                              | Q(user__phone__exact=search)| Q(user__email__iexact=search)
                              | Q(invoice__exact=search))

        if 'user' in self.request.GET:
            qs = qs.filter(user__id=int(self.request.GET.get('user')))

        if 'sort' in self.request.GET:
            if str(self.request.GET.get('sort')) == 'new':
                date_ = datetime.datetime.now()
                qs = qs.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
            elif str(self.request.GET.get('sort')) == 'active':
                qs = qs.filter(active='0')
            elif str(self.request.GET.get('sort')) == 'inactive':
                qs = qs.filter(active='1')
            elif str(self.request.GET.get('sort')) == 'upr':
                qs = qs.filter(upr_status=True)
            elif str(self.request.GET.get('sort')) == 'end':
                end_date = datetime.datetime.now()
                start_date = end_date - datetime.timedelta(weeks=2)
                end = start_date + datetime.timedelta(days=2)
                qs = qs.filter(time_start__lt=start_date)
        return qs

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(TicketListView, self).dispatch(*args, **kwargs)

#Редактирование билета
class DetailTicketView(UpdateView):
    model = YesTicket
    template_name = 'admin/admin-tickets-edit.html'
    fields = ['time_start', 'invoice', 'password', 'server', 'active', 'status', 'upr_status', 'commentary']

    def get_initial(self):
        super(DetailTicketView, self).get_initial()
        times = str(self.get_object().time_start.day) +'.'+str(self.get_object().time_start.month)+'.'+str(self.get_object().time_start.year)
        self.initial = {"time_start":times}
        return self.initial

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(DetailTicketView, self).dispatch(*args, **kwargs)

#Добавления билета
class TicketAddView(CreateView):
    model = YesTicket
    template_name = 'admin/admin-tickets-add.html'
    fields = ['time_start', 'user', 'ticket', 'invoice', 'password', 'server', 'active', 'status', 'upr_status', 'commentary']
    success_url = reverse_lazy('admin-tickets')

    def get_initial(self):
        super(TicketAddView, self).get_initial()
        times = str(self.model().time_start.day) +'.'+str(self.model().time_start.month)+'.'+str(self.model().time_start.year)
        if 'user' in self.request.GET:
            self.initial = {"time_start":times, 'user' : self.request.GET['user']}
        else:
            self.initial = {"time_start":times}
        return self.initial

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(TicketAddView, self).dispatch(*args, **kwargs)

#Удаление билета
class TicketDeleteView(DeleteView):
    model = YesTicket
    template_name = 'admin/admin-сonfirm-delete.html'
    success_url = reverse_lazy('admin-tickets')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(TicketDeleteView, self).dispatch(*args, **kwargs)

#список билетов администратора
class SettingsTicketView(ListView):
    queryset = Ticket.objects.all().order_by('-value')
    template_name = 'admin/admin-tickets-list.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(SettingsTicketView, self).dispatch(*args, **kwargs)

#Редактирование билета администратора
class SettingsTicketEditView(UpdateView):
    model = Ticket
    template_name = 'admin/admin-tickets-list-edit.html'
    fields = '__all__'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(SettingsTicketEditView, self).dispatch(*args, **kwargs)

#Добавление билета администратора
class SettingsTicketAddView(CreateView):
    model = Ticket
    template_name = 'admin/admin-tickets-list-add.html'
    fields = '__all__'
    success_url = reverse_lazy('admin-tickets-settings')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(SettingsTicketAddView, self).dispatch(*args, **kwargs)

#Удаление билета администратора
class SettingsTicketDeleteView(DeleteView):
    model = Ticket
    template_name = 'admin/admin-сonfirm-delete.html'
    success_url = reverse_lazy('admin-tickets-settings')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(SettingsTicketDeleteView, self).dispatch(*args, **kwargs)

#Редактирование курса
class CurencyEditView(UpdateView):
    model = Curency
    template_name = 'admin/admin-curency-edit.html'
    fields = '__all__'
    success_url = reverse_lazy('currency')

    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.
        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        # Next, try looking up by primary key.
        pk = 1
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(CurencyEditView, self).dispatch(*args, **kwargs)

#Редактирование курса
class NotificationEditView(UpdateView):
    model = Notification
    template_name = 'admin/admin-notification-edit.html'
    fields = '__all__'
    success_url = reverse_lazy('admin-notification')

    def get_object(self, queryset=None):
        #send_mail('Subject', 'Here is the message.', 'upnemkov@gmail.com', ['89129244869@mail.ru'], fail_silently=False)
        """
        Returns the object the view is displaying.
        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        # Next, try looking up by primary key.
        pk = 1
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(NotificationEditView, self).dispatch(*args, **kwargs)

#список заявок на вывод по реферальной программе
class ReferalOutView(ListView):
    queryset = BidPayment.objects.all().order_by('-created')
    template_name = 'admin/admin-referal-out.html'

    def get_queryset(self):
        qs = super(ReferalOutView, self).get_queryset()

        if 'q' in self.request.GET:
            search = self.request.GET.get('q')
            qs = qs.filter(Q(user__first_name__iexact=search) | Q(user__last_name__iexact=search)
                              | Q(user__phone__exact=search)| Q(user__email__iexact=search))
        return qs

    def get_context_data(self, **kwargs):
        context = super(ReferalOutView, self).get_context_data(**kwargs)
        context['referals'] = Referal.objects.all()
        return context    

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(ReferalOutView, self).dispatch(*args, **kwargs)

#редактирование заявки обратного звонка
class OrderCallEditView(UpdateView):
    model = OrderCall
    template_name = 'admin/admin-notification-edit.html'
    fields = '__all__'
    success_url = reverse_lazy('admin-bids')

  
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(OrderCallEditView, self).dispatch(*args, **kwargs)

#Редактирование заявки на вывод по реферальной программе
class ReferalOutEditView(UpdateView):
    model = BidPayment
    template_name = 'admin/admin-referal-out-edit.html'
    fields = '__all__'
    success_url = reverse_lazy('affilate-admin')



    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(ReferalOutEditView, self).dispatch(*args, **kwargs)

@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login-admin.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    # Ensure the user-originating redirection url is safe.
    print(redirect_to)
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.user.is_authenticated():
        return redirect(resolve_url(settings.LOGIN_REDIRECT_URL))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    context = {
        'form': form
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

#Оповещение о новых событиях
def notification(request):
    noti = Notification.objects.get(id=1)
    t1 = datetime.datetime.now()
    """
    start_date = datetime.date(2015, 1, 1)
    end_date = datetime.date(2015, 3, 31)
    """
    end_date = datetime.datetime.now()
    start_date = t1 - datetime.timedelta(minutes=1)
    if noti.site:
        tick = []
        usersr = []
        bid = []
        ticket = YesTicket.objects.filter(created__range=(start_date, end_date))
        for tik in ticket:
            tick.append(str(tik.id))
        users = UserProfile.objects.filter(created__range=(start_date, end_date))
        for usr in users:
            usersr.append(str(usr.id))
        bids = BidPayment.objects.filter(created__range=(start_date, end_date))
        for bi in bids:
            bid.append(str(bi.id))
        data = {'error': '0', 'tick': tick, 'usersr': usersr, 'bid': bid, 'ticket': str(len(ticket)), 'users': str(len(users)), 'bids': str(len(bids))}
    else:
        data = {'error': '0'}
    return HttpResponse(json.dumps(data), mimetype="application/json")


#Список обратных звонков
class OrderCallListView(ListView):
    queryset = OrderCall.objects.all().order_by('-created')
    template_name = 'admin/admin-order-call.html'

    def get_context_data(self, **kwargs):
        context = super(OrderCallListView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(OrderCallListView, self).dispatch(*args, **kwargs)

#список страниц
class PageListView(ListView):
    queryset = Article.objects.all().order_by('-created')
    template_name = 'admin/admin-page-list.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(PageListView, self).dispatch(*args, **kwargs)

#Редактирование страницы
class PageListEditView(UpdateView):
    model = Article
    template_name = 'admin/admin-page-list-edit.html'
    fields = ['title', 'slug', 'text', 'publish','name_ref', 'seo_title', 'seo_description', 'seo_keywords',]
    success_url = reverse_lazy('admin-pages')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(PageListEditView, self).dispatch(*args, **kwargs)

#Добавление страницы
class PageListEditAddView(CreateView):
    model = Article
    template_name = 'admin/admin-page-list-add.html'
    fields = ['title', 'slug', 'text', 'publish','name_ref', 'seo_title', 'seo_description', 'seo_keywords',]
    success_url = reverse_lazy('admin-pages')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_manager')
        return super(PageListEditAddView, self).dispatch(*args, **kwargs)

