# -*- coding: utf-8 -*-
from django.utils.translation import get_language
from webaccounts.models import Referal, ReferalHistory, UserProfile
from models import List, Article, Category
from django.views.generic import ListView, DetailView
from partner.models import Links


class ArticleDetail(DetailView):
    """
    Return Article by slug
    """
    queryset = Article.objects.filter(publish=True)
    template_name = 'arcticles/faq_detail.html'

    def get_queryset(self):
        qs = super(ArticleDetail, self).get_queryset()
        #qs = qs.exclude(text_en=u'').exclude(text_en__isnull=True)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        #context['articles'] = Article.objects.filter(article_list__slug='faq', publish=True)
        return context

    def render_to_response(self, context, **response_kwargs):
        response = super(ArticleDetail, self).render_to_response(context, **response_kwargs)
        if 'pl' in self.request.GET and 'l' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            response.set_cookie("l", self.request.GET.get('l'))
            if Referal.objects.get(user_id=int(self.request.GET.get('pl'))):
                users = UserProfile.objects.get(id=int(self.request.GET.get('pl')))
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                #try:
                link = Links.objects.get(id=int(self.request.GET.get('l')))
                ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
        elif 'pl' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            if Referal.objects.get(user_id=int(self.request.GET.get('pl'))):
                users = UserProfile.objects.get(id=int(self.request.GET.get('pl')))
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                #try:
                ReferalHistory.objects.create(ref=ref, price_lid=users.cont_lid)

                #except:
                   #ReferalHistory.objects.create(ref=ref,landing=link[0].id)
        return response

"""
class HelpListView(ListView):

    queryset = Article.objects.filter(article_list__slug='faq', publish=True)
    template_name = 'arcticles/faq_list.html'


    def get_queryset(self):
        qs = super(HelpListView, self).get_queryset()

        #qs = qs.exclude(text_en=u'').exclude(text_en__isnull=True)
        return qs

    def get_context_data(self, **kwargs):
        context = super(HelpListView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context
"""
"""
class FAQListView(ListView):

    queryset = Category.objects.filter(publish=True)
    template_name = 'arcticles/faq_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(FAQListView, self).get_context_data(**kwargs)


        faqs = []
        articles = Article.objects.filter(article_list__title='faq', publish=True)
        if articles:
                faqs.append({'category': '1', 'article_list': articles})
        context['category'] = Category.objects.all()
        context['faqs'] = faqs

        return context
"""