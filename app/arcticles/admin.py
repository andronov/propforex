from django.contrib import admin
from models import *
from django.forms import CheckboxSelectMultiple
from django.utils.translation import ugettext_lazy as _


class ListAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', )
    prepopulated_fields = {"slug": ("title",)}
    formfield_overrides = {
        # models.TextField: {'widget': AdminTinyMCE(attrs={'cols': 80, 'rows': 30}, )},
        }


class ArticleAdmin(ListAdmin):
    list_filter = ('article_list', )
    list_display = ('title', 'slug', 'article_list', 'visitors', 'publish', 'index',)
    search_fields = ('title', )
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }

    fieldsets = [
        [None, {
            'fields': ['title', 'slug', 'article_list', 'short_text', 'text', 'publish', 'index', ]
        }],
        [_('Extra fields'), {
            'fields': ['category', 'seo_title', 'seo_description', 'seo_keywords'],
            'classes': ['collapse'],
        }]
    ]

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            '/static/grappelli_modeltranslation/js/force_jquery.js',
            '/static/grappelli_modeltranslation/js/tabbed_translation_fields.js',
        ]
        css = {
            'screen': ('/static/grappelli_modeltranslation/css/tabbed_translation_fields.css',),
        }


class TagAdmin(ListAdmin):
    pass


class CategoryAdmin(ListAdmin):

    class Media:
        js = [
            '/static/grappelli/tinymce/jscripts/tiny_mce/tiny_mce.js',
            '/static/grappelli/tinymce_setup/tinymce_setup.js',
            '/static/grappelli_modeltranslation/js/force_jquery.js',
            '/static/grappelli_modeltranslation/js/tabbed_translation_fields.js',
        ]
        css = {
            'screen': ('/static/grappelli_modeltranslation/css/tabbed_translation_fields.css',),
        }


admin.site.register(List, ListAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Category, CategoryAdmin)