# -*- coding: utf-8 -*-
import datetime
import json
import urllib2
from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMultiAlternatives
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, resolve_url, render_to_response, get_object_or_404

# Create your views here.
from django.template import RequestContext, TemplateDoesNotExist
from django.template.defaulttags import csrf_token
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_protect
from django.views.generic import TemplateView, ListView, DeleteView
from registration.models import RegistrationProfile
import requests
from robokassa.forms import RobokassaForm
from robokassa.signals import result_received, success_page_visited
from accounts.forms import _createHash
from webaccounts.models import Referal, ReferalHistory, BidPayment, Curency
from accounts.models import  UserProfile as UserProfileGlav
from core.models import Ticket, YesTicket, OrderCall, Order
from partner.models import Links
from accounts.models import Referal as  AReferal , ReferalHistory as AReferalHistory, BidPayment as ABidPayment, Curency as ACurency

class HomePageView(TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        return context


    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageView, self).render_to_response(context, **response_kwargs)
        if 'pl' in self.request.GET and 'l' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            response.set_cookie("l", self.request.GET.get('l'))
            if Referal.objects.get(user_id=int(self.request.GET.get('pl'))):
                try:
                    users = UserProfileGlav.objects.get(id=int(self.request.GET.get('pl')))
                    ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                    refs = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                    link = Links.objects.get(id=int(self.request.GET.get('l')))
                    ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
                    AReferalHistory.objects.create(ref=refs)
                except:
                    users = UserProfileGlav.objects.get(id=int(self.request.GET.get('pl')))
                    ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                    link = Links.objects.get(id=int(self.request.GET.get('l')))
                    ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
        elif 'pl' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            s = Referal.objects.filter(user_id=int(self.request.GET.get('pl')))
            t = AReferal.objects.filter(user_id=int(self.request.GET.get('pl')))
            if s and t:
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                refs = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                ReferalHistory.objects.create(ref=ref)
                AReferalHistory.objects.create(ref=refs)
            elif s:
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                ReferalHistory.objects.create(ref=ref)
            elif t:
                ref = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                AReferalHistory.objects.create(ref=ref)
            else:
                pass
        return response

class HomePageEnView(TemplateView):
    template_name = 'core/home-en.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageEnView, self).get_context_data(**kwargs)
        return context


    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageEnView, self).render_to_response(context, **response_kwargs)
        if 'pl' in self.request.GET and 'l' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            response.set_cookie("l", self.request.GET.get('l'))
            if Referal.objects.get(user_id=int(self.request.GET.get('pl'))):

                try:
                    users = UserProfileGlav.objects.get(id=int(self.request.GET.get('pl')))
                    ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                    refs = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                    link = Links.objects.get(id=int(self.request.GET.get('l')))
                    ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
                    AReferalHistory.objects.create(ref=refs)
                except:
                    users = UserProfileGlav.objects.get(id=int(self.request.GET.get('pl')))
                    ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                    link = Links.objects.get(id=int(self.request.GET.get('l')))
                    ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
        elif 'pl' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            s = Referal.objects.filter(user_id=int(self.request.GET.get('pl')))
            t = AReferal.objects.filter(user_id=int(self.request.GET.get('pl')))
            if s and t:
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                refs = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                ReferalHistory.objects.create(ref=ref)
                AReferalHistory.objects.create(ref=refs)
            elif s:
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                ReferalHistory.objects.create(ref=ref)
            elif t:
                ref = AReferal.objects.get(user_id=int(self.request.GET.get('pl')))
                AReferalHistory.objects.create(ref=ref)
            else:
                pass
        return response

class CabinetView(TemplateView):
    template_name = 'core/cabinet-view.html'

    def get_context_data(self, **kwargs):
        context = super(CabinetView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(CabinetView, self).dispatch(*args, **kwargs)

class MaterialView(TemplateView):
    template_name = 'core/cabinet-material.html'

    def get_context_data(self, **kwargs):
        context = super(MaterialView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(MaterialView, self).dispatch(*args, **kwargs)

#Информация
class CabinetRulesView(TemplateView):
    template_name = 'core/cabinet-rules.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(CabinetRulesView, self).dispatch(*args, **kwargs)

class BuyTicketView(ListView):
    template_name = 'core/cabinet-buy-ticket.html'
    queryset = Ticket.objects.filter(publish=True).order_by('value')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(BuyTicketView, self).dispatch(*args, **kwargs)

#Активные билеты
class ActiveTicketView(ListView):
    template_name = 'core/cabinet-list-ticket.html'
    queryset = YesTicket.objects.all().order_by('-created')

    def get_queryset(self):
        qs = super(ActiveTicketView, self).get_queryset()
        qs = qs.filter(active=0, user=self.request.user)
        return qs

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(ActiveTicketView, self).dispatch(*args, **kwargs)

#Неактивные билеты
class InActiveTicketView(ListView):
    template_name = 'core/cabinet-list-inticket.html'
    queryset = YesTicket.objects.all().order_by('-created')

    def get_queryset(self):
        qs = super(InActiveTicketView, self).get_queryset()
        qs = qs.filter(active=1, user=self.request.user)
        return qs

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(InActiveTicketView, self).dispatch(*args, **kwargs)

#Удаление заявки на вывод
def bid_delete(request, id):
    bid = ABidPayment.objects.get(id=id)
    bid.delete()
    return redirect('affilate')

from dateutil.relativedelta import relativedelta
#Партнерка
class AffilateView(TemplateView):
    template_name = 'core/affilate.html'

    def get_context_data(self, **kwargs):
        context = super(AffilateView, self).get_context_data(**kwargs)
        ref = AReferal.objects.get(user_id=self.request.user.id)
        refer = AReferalHistory.objects.filter(ref=ref)
        refer_arr2 = []
        for re in refer:
            refer_arr2.append(re.id)
        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-7':
                end_date = datetime.datetime.now()
                start_date = end_date - datetime.timedelta(weeks=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-365':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(years=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
        else:
            date_ = datetime.datetime.now()
            refer = refer.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)

        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)



        bid = 0
        bids = ABidPayment.objects.filter(user=self.request.user)
        for bd in bids:
            bid = bid + bd.sum

        context['ref_list'] = refer
        context['ref'] = ref
        context['dates'] = date_
        context['all_refer'] = ref.all_refer(refer_arr)
        context['count_buy'] = ref.count_buy(refer_arr)
        context['summa_buy'] = ref.summa_buy(refer_arr)
        context['amount_buy_two'] = ref.amount_buy_two(refer_arr)
        context['epct'] = ref.epc(refer_arr)
        context['epc'] = ref.amount_buy(refer_arr2)
        context['job_out'] = bid
        context['balance'] = ref.amount_buy(refer_arr2) - bid
        context['historys'] = ABidPayment.objects.filter(user=self.request.user).order_by('-created')
        context['users'] = UserProfileGlav.objects.get(id=self.request.user.id)
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        status = False
        sistema = False
        ref = AReferal.objects.get(user_id=self.request.user.id)
        users = UserProfileGlav.objects.get(id=self.request.user.id)
        refer = AReferalHistory.objects.filter(ref=ref)
        bid = 0
        bids = ABidPayment.objects.filter(user=self.request.user)
        for bd in bids:
            bid = bid + bd.sum
            if bd.result == 1 or bd.result == 0:
               status = True
        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)
        if users.sistemas == 0 or users.sistemas == 1:
            sistema = False
        else:
            sistema = True    
        if 'summa' in request.POST:
          summa = request.POST.get('summa')
          try:
              int(summa)
          except:
              context['errors'] = 'Введите число'
              return self.render_to_response(context)
          if not summa:
            context['errors'] = 'Введите сумму'
          elif summa == '0':
            context['errors'] = 'Сумма не может быть равна 0'
          elif int(summa) < int(settings.MONEY_OUT):
            context['errors'] = 'Сумму на вывод должна быть от ' + str(settings.MONEY_OUT) + '$'
          elif int(summa) > int(ref.amount_buy(refer_arr) - bid):
            context['errors'] = 'Ошибка! Сумма больше возможной на вывод'
          elif status:
            context['errors'] = 'У Вас уже есть созданные заявки!'
          elif sistema:
            context['errors'] = 'Сначала добавьте систему вывода!'
          else:
            try:
               ABidPayment.objects.create(user=self.request.user, sum=int(summa))
               context['yesout'] = True
            except:
               context['yesout'] = False
          return self.render_to_response(context)
        if 'number' in request.POST:
            number = request.POST.get('number')
            sistema = request.POST.get('sistema')
            if not number or not sistema:
               context['errorss'] = 'Введите данные'
            else:
                users.sistemas = sistema
                users.number_sistem = number
                users.save()
                context['success'] = 'Система вывода успешно добавлена!'
            return self.render_to_response(context)

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        elif self.request.user.is_webmaster:
            return redirect('auth_login')
        else:
            if not self.request.user.is_go:
                return redirect('next_registration')
        return super(AffilateView, self).dispatch(*args, **kwargs)


#Редактирование пользователя
def edituser(request):
    context = RequestContext(request)

    if not request.user.is_authenticated():
        return redirect(resolve_url(settings.LOGIN_REDIRECT_URL))
    users = UserProfileGlav.objects.get(id=request.user.id)
    context['users'] = users
    if 'notification' in request.GET:
        noti = request.GET['notification']
        if str(noti) == 'True':
            users.send = True
            users.save()
        elif str(noti) == 'False':
            users.send = False
            users.save()
            context['noti_false'] = "Оповещение о платежах приостановлено!"
        else:
            pass
    if request.POST:
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        phone = request.POST.get('phone')
        if first_name and last_name and phone:
            user = UserProfileGlav.objects.get(id=request.user.id)
            user.first_name = first_name
            user.last_name = last_name
            user.phone = phone
            user.save()
            return redirect(resolve_url('user-edit'))
        else:
            context['yesout'] = False

    return render_to_response('core/cabinet-user-edit.html', context)

#отписка от рассылки
def cancel_send(request, email=None):
    context = RequestContext(request)
    users = UserProfileGlav.objects.get(email=email)
    context['users'] = users
    if 'notification' in request.GET:
        noti = request.GET['notification']
        if str(noti) == 'True':
            users.send = True
            users.save()
        elif str(noti) == 'False':
            users.send = False
            users.save()
            context['noti_false'] = "Оповещение о платежах приостановлено!"
        else:
            pass
    return render_to_response('core/cabinet-cancel-send.html', context)

def logout_view(request):
    logout(request)
    return redirect('home')

#заказ обратного звонка
def order_call(request):
    data = {}
    name = request.POST['name']
    phone = request.POST['phone']
    if not name or not phone:
        data['error'] = 'Заполните все поля!'
    else:
        OrderCall.objects.create(name=name, phone=phone)
        data['ok'] = 'Спасибо Ваша заявка принята!'
    return HttpResponse(json.dumps({'data':data}), content_type="application/json")

@login_required
def pay_with_robokassa(request, ticket_id):
    ticket = Ticket.objects.get(id=ticket_id)
    curen = Curency.objects.get(id=1)
    itogo = ticket.value * curen.value
    ord = Order.objects.create(user=request.user,ticket=ticket,amount=itogo)

    order = get_object_or_404(Order, id=ord.id)

    form = RobokassaForm(initial={
               'OutSum': order.amount,
               'InvId': order.id,
               #'Desc': order.name,
               'Email': request.user.email,
               # 'IncCurrLabel': '',
               # 'Culture': 'ru'
           })

    return render(request, 'core/pay_with_robokassa.html', {'form': form, 'summa': itogo})


def payment_received(sender, **kwargs):
    order = Order.objects.get(id=kwargs['InvId'])
    order.check = True
    order.amount = 0
    order.save()

result_received.connect(payment_received)


import mimetypes
import os
def sert(request):
    BASE_DIR = os.path.dirname('yandex_67fb7d7c49fd886a.txt')
    print(BASE_DIR)
    path = '/home/propforex/yandex_67fb7d7c49fd886a.txt'
    fsock = open(path,"rb")

    response = HttpResponse(fsock, content_type = mimetypes.guess_type(path)[0])
    response['Content-Length'] = os.path.getsize(path) # not FileField instance
    response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(path)# same here
    return response

import hashlib
import time
def _createPass():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:6]

#Лэндинг LP1
@csrf_protect
def new_landing_user(request):
    data = {}
    if request.POST:
        email = request.POST['msg']

        if email:
          try:
              UserProfileGlav.objects.get(email=email)
              data['error'] = 'Пользователь с таким электронным адресом уже существует!!'
              return HttpResponse(json.dumps({'data':data}), content_type="application/json")
          except UserProfileGlav.DoesNotExist:
            new_user = RegistrationProfile.objects.create_inactive_user(username=_createHash(),
                        first_name=u'Удачливый',
                        last_name=u'Трейдер',
                        password=_createPass(),
                        email=email,
                        site='',
                        lp='lp')

            print(new_user)

            user = UserProfileGlav.objects.get(id=new_user.id)
            AReferal.objects.create(user_id=user.id)
            Referal.objects.create(user_id=user.id)
            #print(ref)
            if 'ref' in request.COOKIES and 'l' in request.COOKIES:
                     refffer = Referal.objects.get(user_id=int(request.COOKIES['ref']))
                     link = Links.objects.get(id=int(request.COOKIES['l']))
                     refer = ReferalHistory.objects.create(ref=refffer,landing=link,refer=new_user, price_lid=user.cont_lid)
                     referd = AReferalHistory.objects.create(ref=refffer, refer=user)
                     print(refer)

                     #ReferalHistory.objects.create(ref=reffffer,landing=link,refer=new_user, price_lid=user.cont_lid)
            elif 'ref' in request.COOKIES:
                     refffer = Referal.objects.get(user_id=int(request.COOKIES['ref']))
                     refer = ReferalHistory.objects.create(ref=refffer, refer=user)
                     referd = AReferalHistory.objects.create(ref=refffer, refer=user)


            """
            text_content = 'Данные с доступом к счету активного билета'
            html_content = render_to_string('mailchimp/notification.html', {'users': users, 'ticket': ticket})
            msg = EmailMultiAlternatives('Данные с доступом к счету активного билета', text_content, settings.EMAIL_HOST_USER,  [users.email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            """
            data['ok'] = 'Спасибо Ваша заявка принята!'

        else:
           data['error'] = 'Вы ввели неправильный адрес электронной почты!'
    else:
        data['error'] = 'Вы ввели неправильный адрес электронной почты!'
    return HttpResponse(json.dumps({'data':data}), content_type="application/json")


#Продолжение регистрации
def next(request):
    context = RequestContext(request)

    if request.user.is_webmaster:
         return redirect('auth_login')
    elif request.user.is_go:
         return redirect('auth_login')
    user = UserProfileGlav.objects.get(id=request.user.id)
    if request.POST:
        print(request.POST)
        try:
            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.otchestvo = request.POST['otchestvo']
            user.country = request.POST['country']
            user.city = request.POST['city']
            user.birthday = request.POST['birthday']
            user.phone = request.POST['phone']
            user.is_go = True
            user.save()
            full_list_user(user.id)
            return redirect('cabinet-rules')
        except:
            context['err'] = u'Ошибка! Проверьте введенные данные!'
            return render_to_response('core/cabinet_next.html', context)
    return render_to_response('core/cabinet_next.html', context)


def full_list_user(id):
    try:
           userr = UserProfileGlav.objects.get(id=id)
           name = ''
           if userr.first_name:
               name = userr.get_full_name()
           payload = {'name': name, 'email': userr.email, 'list': settings.FULLUSERS_LIST}
           r = requests.post("http://mail.propforex.co/subscribe", data=payload)
           print(r)
    except:
        pass