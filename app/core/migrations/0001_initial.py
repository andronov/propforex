# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Ticket'
        pass


    def backwards(self, orm):
        # Deleting model 'Ticket'
        pass


    models = {
        u'core.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'amount': ('django.db.models.fields.PositiveIntegerField', [], {'default': '1000'}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '14'}),
            'goal': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'sag_max': ('django.db.models.fields.IntegerField', [], {'default': '20'}),
            'sag_min': ('django.db.models.fields.IntegerField', [], {'default': '50'}),
            'value': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['core']