from django.contrib import admin

# Register your models here.
from core.models import *

admin.site.register(Ticket)
admin.site.register(YesTicket)
admin.site.register(OrderCall)
admin.site.register(Order)