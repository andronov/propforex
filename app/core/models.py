# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils import timezone
from django.db import models

# Create your models here.
import requests
from accounts import UserProfile
from accounts.models import Notification
from webaccounts.models import ReferalHistory, UserProfile as WB


def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)

class Ticket(models.Model):
    value = models.PositiveIntegerField(default=0, verbose_name=u'Стоимость билета')
    duration = models.PositiveIntegerField(default=14, verbose_name=u'Длительность отбора')
    goal = models.PositiveIntegerField(default=100, verbose_name=u'Цель по прибыли')
    sag_max = models.IntegerField(default=20, verbose_name=u'Макс.дневная просадка')
    sag_min = models.IntegerField(default=50, verbose_name=u'Максимальная просадка')
    amount = models.PositiveIntegerField(default=1000, verbose_name=u'Сумма в управление')
    publish = models.BooleanField(verbose_name="Опубликовано", default=True)

    def __unicode__(self):
            return self.value

class YesTicket(models.Model):
    TYPE_ONE = 0
    TYPE_TWO = 1

    TYPE_SYSTEM = (
        (TYPE_ONE, u'Активный'),
        (TYPE_TWO, u'Неактивен'),
    )
    STATUS_ONE = 1
    STATUS_TWO = 2
    STATUS_THREE = 3
    STATUS_FOUR = 4
    STATUS_FIVE = 5
    STATUS_SIX = 6
    STATUS_SEVEN = 7

    TYPE_STATUS = (
        (STATUS_ONE, u'Превышено дневное ограничение по просадке'),
        (STATUS_TWO, u'Превышена максимально допустимая просадка'),
        (STATUS_THREE, u'Цель по прибыли не выполнена'),
        (STATUS_FOUR, u'Количество прибыльных дней не соответствует условиям'),
        (STATUS_FIVE, u'Использованы запрещённые торговые системы'),
        (STATUS_SIX, u'Установлено дополнительное испытание'),
		(STATUS_SEVEN, u'Бесплатный билет/"Второй шанс"'),
    )
    UPR_ONE = 0
    UPR_TWO = 1

    TYPE_UPR = (
        (UPR_ONE, u'Первый вариант'),
        (UPR_TWO, u'Второй')
    )
    time_start = models.DateTimeField(default=timezone.now, blank=True, null=True, verbose_name=u'Дата старта')
    user = models.ForeignKey(UserProfile, related_name='user_ticket', verbose_name=u'Пользователь')
    ticket = models.ForeignKey(Ticket, related_name='yes_ticket', verbose_name=u'Билет')
    invoice = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'Счет брокера')
    password = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'Пароль')
    server = models.CharField(max_length=50, default='ConcordBay-DemoUK', blank=True, null=True, verbose_name=u'Сервер')
    active = models.IntegerField(choices=TYPE_SYSTEM, blank=True, null=True, default=0, verbose_name=u'Активность')
    status = models.IntegerField(choices=TYPE_STATUS, blank=True, null=True, verbose_name=u'Статус билета')

    order = models.IntegerField(default=0,blank=True,null=True)
    send = models.BooleanField(default=False)
    upr_status = models.BooleanField(default=False, verbose_name=u'Управляющий билет')

    commentary = models.TextField(max_length=1200, blank=True, null=True, verbose_name=u'Комментарий')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def com(self):
        if len(self.commentary) >= 30:
            return self.commentary[:30] + '...'
        else:
            return self.commentary[:30]

    def time_finish(self):
        return self.time_start + datetime.timedelta(weeks=2)

    def all_amounts(self):
        return self.ticket.value

    def save(self, *args, **kwargs):
        dt = datetime.datetime.now()
        if getattr(self, 'time_start', True):
            pass
        else:
            self.time_start = next_weekday(dt, 0)
        super(YesTicket, self).save()


class OrderCall(models.Model):
    TYPE_WHO = 1
    TYPE_REP = 2
    TYPE_LIST = 3

    TYPE_CHOICES = (
        (TYPE_WHO, 'Ожидает'),
        (TYPE_REP, 'Не дозвонились'),
        (TYPE_LIST, 'Завершен'),
    )
    result = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True, default=1, verbose_name=u'Статус')
    name = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'Имя')
    phone = models.CharField(max_length=50, blank=True, null=True, verbose_name=u'Телефон')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Order(models.Model):
    user = models.ForeignKey(UserProfile, related_name='order_user', verbose_name=u'юзер')
    ticket = models.ForeignKey(Ticket, related_name='order_ticket', verbose_name=u'Билет')
    amount = models.PositiveIntegerField(verbose_name=u'Сумма в оплаты в рублях')
    check = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.check:
            yes = YesTicket()
            yes.order = self.id
            yes.user = self.user
            yes.ticket = self.ticket
            yes.save()
        super(Order, self).save()


#Отправка оповещение после добавления билета
def update_stock(sender, instance, **kwargs):
    if kwargs["created"]:
        noti = Notification.objects.get(id=1)
        if noti.pochta:
           users = UserProfile.objects.get(id=instance.user.id)
           try:
               ref =  ReferalHistory.objects.get(refer=users)
               con = WB.objects.get(id=ref.ref.user_id)
               if ref.result != 2:
                   ref.result = 2
                   ref.price_lid = con.cont_lid
                   ref.save()
           except:
                pass
           text_content = 'Куплен новый билет'
           html_content = render_to_string('mailchimp/ticket.html', {'users': users})
           msg = EmailMultiAlternatives('Куплен новый билет', text_content, settings.EMAIL_HOST_USER,  [noti.email])
           msg.attach_alternative(html_content, "text/html")
           msg.send()

# register the signal
post_save.connect(update_stock, sender=YesTicket, dispatch_uid="update_stock_count")

#Отправка оповещение после добавления обратного звонка
def call_stock(sender, instance, **kwargs):
    if kwargs["created"]:
        noti = Notification.objects.get(id=1)
        if noti.pochta:
           users = UserProfile.objects.get(username='admin')
           text_content = 'Пользователь заказал обратный звонок'
           html_content = render_to_string('mailchimp/call.html', {'users': users})
           msg = EmailMultiAlternatives('Пользователь заказал обратный звонок', text_content, settings.EMAIL_HOST_USER,  [noti.email])
           msg.attach_alternative(html_content, "text/html")
           msg.send()

# register the signal
post_save.connect(call_stock, sender=OrderCall, dispatch_uid="call_stock_count")


#add date
def update_stocks(sender, instance, **kwargs):
    if kwargs["created"]:
        dt = datetime.datetime.now()
        yes = YesTicket.objects.get(id=instance.id)
        yes.time_start = next_weekday(dt, 0)
        yes.save()


# register the signal
post_save.connect(update_stocks, sender=YesTicket, dispatch_uid="update_stock_count")



#Отправка add_listbuytciket
def add_listbuytciket(sender, instance, **kwargs):
    if kwargs["created"]:
        try:
           userr = UserProfile.objects.get(id=instance.user.id)
           name = ''
           if userr.first_name:
               name = userr.get_full_name()
           payload = {'name': name, 'email': userr.email, 'list': settings.BUYTICKET_LIST}
           r = requests.post("http://mail.propforex.co/subscribe", data=payload)
           print(r)
        except:
            pass

# register the signal
post_save.connect(add_listbuytciket, sender=YesTicket, dispatch_uid="add_listbuytciket")
