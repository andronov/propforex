# -*- coding: utf-8 -*-
from django.contrib import admin

from webaccounts.models import *

# Register your models here.
class ReferalHistoryAdmin(admin.ModelAdmin):
    model = ReferalHistory

    #prepopulated_fields = {"slug": ('slug',)}
    list_display = ['ref', 'refer', 'transition', 'result', 'landing']
    search_fields = ('ref', 'refer')

    def ref(self, instance):
        return instance.ref.username
    def refer(self, instance):
        return instance.refer.username
    def landing(self, instance):
        return instance.landing.name

    landing.short_description = 'landing'
    ref.short_description = 'User'
    refer.short_description = 'him user'

admin.site.register(UserProfile)
admin.site.register(Referal)
admin.site.register(ReferalHistory, ReferalHistoryAdmin)
admin.site.register(BidPayment)
admin.site.register(Notification)
admin.site.register(Curency)