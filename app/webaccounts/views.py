from django.conf import settings
from django.contrib.sites.models import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url, redirect, render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
            logout as auth_logout, get_user_model)
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from webaccounts import UserProfile
from accounts import UserProfile as UserProfileGlav
from webaccounts.forms import CustomUserRegistrationForm, _createHash, CustomAuthenticationForm
from webaccounts.models import Referal
from webaccounts.models import ReferalHistory
from partner.models import Links


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=CustomAuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    # Ensure the user-originating redirection url is safe.
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.user.is_authenticated():
        return redirect(resolve_url(settings.LOGIN_REDIRECT_URL))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    context = {
        'form': form
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


