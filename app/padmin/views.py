# -*- coding: utf-8 -*-
import datetime
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
            logout as auth_logout, get_user_model)
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect, resolve_url, render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import TemplateView, ListView, UpdateView, CreateView, DeleteView
from webaccounts.models import BidPayment, Referal, Notification, ReferalHistory, UserProfile
from django.utils.translation import ugettext_lazy as _
from arcticles.models import Article
from partner.models import Links
from datetime import datetime as dt
from accounts.models import UserProfile as UserProfileGlav

class HomeStatView(TemplateView):
    template_name = 'padmin/padmin-stat.html'

    def get_context_data(self, **kwargs):
        context = super(HomeStatView, self).get_context_data(**kwargs)
        return context

    """
    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageView, self).render_to_response(context, **response_kwargs)
        if 'pl' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            if Referal.objects.get(user_id=int(self.request.GET.get('pl'))):
                ref = Referal.objects.get(user_id=int(self.request.GET.get('pl')))
                ReferalHistory.objects.create(ref=ref)
        return response
    """

#Статистика пользователя padmin
class StatPadminView(TemplateView):
    template_name = 'padmin/padmin-stats.html'

    def get_context_data(self, **kwargs):
        context = super(StatPadminView, self).get_context_data(**kwargs)
        users = UserProfile.objects.filter(is_webmaster=True)
        usss = []
        for us in users:
            usss.append(us.id)
        reft = []
        refs = Referal.objects.filter(user_id__in=usss)
        for ref in refs:
            reft.append(ref.id)

        refer = ReferalHistory.objects.filter(ref__in=reft)
        context['links'] = Links.objects.filter(user_id=self.request.user.id)

        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
                refer_arr = []
                ends_d = datetime.datetime.now()
                date_dat = [ends_d]
                starts_d = end_date - relativedelta(months=1)
                while ends_d >= starts_d:
                   ends_d -= datetime.timedelta(days=1)
                   #print(ends_d - datetime.timedelta(days=1))
                   date_dat.append(ends_d)
                print(date_dat)

                for re in refer:
                    refer_arr.append(re.id)
                ref = Referal()
                context['week'] = True
                context['ref_list'] = refer
                context['date_dats'] = date_dat
                context['visit'] = refer.count()
                context['leads'] = ref.all_refer_admin(refer_arr)
                context['waits'] = ref.all_wait_admin(refer_arr)
                context['received'] = ref.all_received_admin(refer_arr)
                context['output'] = ref.all_output_admin(refer_arr)
                context['partnerka'] = ref.summa_buy_partner(refer_arr)
                context['epc'] = ref.epc_partner(refer_arr)
                context['summaadmin'] = ref.summa_buy_partner_admin(refer_arr)
                context['users'] = users
                return context

        elif 'time_start' in self.request.GET and 'time_finish' in self.request.GET and 'channel' in self.request.GET:
            time_start, time_finish = self.request.GET.get('time_start'), self.request.GET.get('time_finish')
            time_start_dt = dt.strptime(time_start, "%d.%m.%Y")
            time_finish_dt = dt.strptime(time_finish, "%d.%m.%Y")
            if self.request.GET.get('channel'):
               link = Links.objects.get(id=self.request.GET.get('channel'))

            date_dat = [time_start_dt]
            while time_start_dt <= time_finish_dt:
               time_start_dt += datetime.timedelta(days=1)
               date_dat.append(time_start_dt)
            date_dat.pop()

            start_date = datetime.date(time_start_dt.year, time_start_dt.month, time_start_dt.day)
            if self.request.GET.get('channel'):
                refer = refer.filter(created__range=(date_dat[0],start_date),landing=link)
            else:
                refer = refer.filter(created__range=(date_dat[0],start_date))

            refer_arr = []
            for re in refer:
                refer_arr.append(re.id)
            ref = Referal()

            context['time_start'] = time_start
            context['time_finish'] = time_finish
            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_dat
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer_admin(refer_arr)
            context['waits'] = ref.all_wait_admin(refer_arr)
            context['received'] = ref.all_received_admin(refer_arr)
            context['output'] = ref.all_output_admin(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            context['summaadmin'] = ref.summa_buy_partner_admin(refer_arr)
            context['users'] = users
            return context

        elif 'time_start' in self.request.GET and 'time_finish' in self.request.GET:
            time_start, time_finish = self.request.GET.get('time_start'), self.request.GET.get('time_finish')
            time_start_dt = dt.strptime(time_start, "%d.%m.%Y")
            time_finish_dt = dt.strptime(time_finish, "%d.%m.%Y")

            date_dat = [time_start_dt]
            while time_start_dt <= time_finish_dt:
               time_start_dt += datetime.timedelta(days=1)
               date_dat.append(time_start_dt)
            date_dat.pop()

            start_date = datetime.date(time_start_dt.year, time_start_dt.month, time_start_dt.day)
            refer = refer.filter(created__range=(date_dat[0],start_date))

            refer_arr = []
            for re in refer:
                refer_arr.append(re.id)
            ref =  Referal()

            context['time_start'] = time_start
            context['time_finish'] = time_finish
            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_dat
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer_admin(refer_arr)
            context['waits'] = ref.all_wait_admin(refer_arr)
            context['received'] = ref.all_received_admin(refer_arr)
            context['output'] = ref.all_output_admin(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            context['summaadmin'] = ref.summa_buy_partner_admin(refer_arr)
            context['users'] = users
            return context

        else:
            end_date = datetime.datetime.now()
            start_date = end_date - datetime.timedelta(weeks=1)
            date_ = start_date
            refer = refer.filter(created__range=(start_date, end_date))
            refer_arr = []
            ends_d = datetime.datetime.now()
            date_dat = [ends_d]
            starts_d = ends_d - datetime.timedelta(weeks=1)
            while ends_d >= starts_d:
               ends_d -= datetime.timedelta(days=1)
               #print(ends_d - datetime.timedelta(days=1))
               date_dat.append(ends_d)
            print(date_dat)

            for re in refer:
                refer_arr.append(re.id)
            ref = Referal()

            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_dat
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer_admin(refer_arr)
            context['waits'] = ref.all_wait_admin(refer_arr)
            context['received'] = ref.all_received_admin(refer_arr)
            context['output'] = ref.all_output_admin(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            context['summaadmin'] = ref.summa_buy_partner_admin(refer_arr)
            context['users'] = users
            return context



        """
        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-7':
                end_date = datetime.datetime.now()
                start_date = end_date - datetime.timedelta(weeks=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-365':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(years=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
        else:
            end_date = datetime.datetime.now()
            start_date = end_date - datetime.timedelta(weeks=1)
            date_ = start_date
            refer = refer.filter(created__range=(start_date, end_date))
            #date_ = datetime.datetime.now()
            #refer = refer.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)

        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)


        context['ref_list'] = refer
        context['ref'] = ref
        context['dates'] = date_
        context['all_refer'] = ref.all_refer(refer_arr)
        context['count_buy'] = ref.count_buy(refer_arr)
        context['summa_buy'] = ref.summa_buy(refer_arr)
        context['amount_buy_two'] = ref.amount_buy_two(refer_arr)
        context['epct'] = ref.epc(refer_arr)
        context['epc'] = ref.amount_buy(refer_arr2)
        return context
        """

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(StatPadminView, self).dispatch(*args, **kwargs)

#список заявок на вывод по реферальной программе
class ReferalOutPadminView(ListView):
    queryset = BidPayment.objects.all().order_by('-created')
    template_name = 'padmin/padmin-output.html'

    def get_queryset(self):
        qs = super(ReferalOutPadminView, self).get_queryset()

        if 'q' in self.request.GET:
            search = self.request.GET.get('q')
            qs = qs.filter(Q(user__first_name__iexact=search) | Q(user__last_name__iexact=search)
                              | Q(user__phone__exact=search)| Q(user__email__iexact=search))
        return qs

    def get_context_data(self, **kwargs):
        context = super(ReferalOutPadminView, self).get_context_data(**kwargs)
        context['referals'] = Referal.objects.all()
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(ReferalOutPadminView, self).dispatch(*args, **kwargs)

#Редактирование заявки на вывод по реферальной программе
class ReferalOutEditPadminView(UpdateView):
    model = BidPayment
    template_name = 'padmin/padmin-output-edit.html'
    fields = '__all__'
    success_url = reverse_lazy('padmin-output')



    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(ReferalOutEditPadminView, self).dispatch(*args, **kwargs)

#Редактирование оповещение
class NotificationEditPadminView(UpdateView):
    model = Notification
    template_name = 'padmin/padmin-notification.html'
    fields = '__all__'
    success_url = reverse_lazy('padmin-notification')

    def get_object(self, queryset=None):
        #send_mail('Subject', 'Here is the message.', 'upnemkov@gmail.com', ['89129244869@mail.ru'], fail_silently=False)
        """
        Returns the object the view is displaying.
        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        # Next, try looking up by primary key.
        pk = 1
        if pk is not None:
            queryset = queryset.filter(pk=pk)

        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(NotificationEditPadminView, self).dispatch(*args, **kwargs)

#список страниц
class PageListPadminView(ListView):
    queryset = Article.objects.all().order_by('-created')
    template_name = 'padmin/padmin-pages.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(PageListPadminView, self).dispatch(*args, **kwargs)

#Редактирование страницы
class PageListEditPadminView(UpdateView):
    model = Article
    template_name = 'padmin/padmin-pages-edit.html'
    fields = ['title', 'slug', 'text', 'publish','name_ref', 'seo_title', 'seo_description', 'seo_keywords',]
    success_url = reverse_lazy('padmin-pages')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(PageListEditPadminView, self).dispatch(*args, **kwargs)

#Добавление страницы
class PageListEditAddPadminView(CreateView):
    model = Article
    template_name = 'padmin/padmin-pages-add.html'
    fields = ['title', 'slug', 'text', 'publish','name_ref', 'seo_title', 'seo_description', 'seo_keywords',]
    success_url = reverse_lazy('padmin-pages')

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(PageListEditAddPadminView, self).dispatch(*args, **kwargs)

#Лиды
class LeadsListPadminView(ListView):
    template_name = 'padmin/padmin-list.html'
    queryset = ReferalHistory.objects.all().order_by('-created')

    def get_queryset(self):
        qs = super(LeadsListPadminView, self).get_queryset()
        users = UserProfile.objects.filter(is_webmaster=True,is_active=True)
        user = []
        for us in users:
            user.append(us.id)
        ref = Referal.objects.filter(user_id__in=user)
        refs = []
        for ret in ref:
            refs.append(ret.id)
        qs = qs.filter(refer__isnull=False,ref__in=refs)
        return qs

    def get_context_data(self, **kwargs):
        context = super(LeadsListPadminView, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(publish=True)
        return context


#Список Пользователей
class UsersPAdminView(ListView):
    queryset = UserProfile.objects.filter(is_webmaster=True).order_by('-created')
    template_name = 'padmin/padmin-users.html'

    def get_queryset(self):
        qs = super(UsersPAdminView, self).get_queryset()
        if 'q' in self.request.GET:
            search = self.request.GET.get('q')
            qs = qs.filter(Q(first_name__iexact=search) | Q(last_name__iexact=search)
                              | Q(phone__exact=search)| Q(email__iexact=search))
        return qs

    def get_context_data(self, **kwargs):
        context = super(UsersPAdminView, self).get_context_data(**kwargs)
        date_ = datetime.datetime.now()
        users = UserProfile.objects.filter(is_webmaster=True, created__day=date_.day, created__month=date_.month, created__year=date_.year).count()
        context['users'] = users
        context['referals'] = Referal.objects.all()
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(UsersPAdminView, self).dispatch(*args, **kwargs)

#Редактирование Пользователя
class UsersPAdminEditView(UpdateView):
    model = UserProfile
    template_name = 'padmin/padmin-users-edit.html'
    fields = ['first_name', 'phone', 'email', 'is_active','cont_lid', 'sistemas', 'number_sistem']

    def get_initial(self):
        super(UsersPAdminEditView, self).get_initial()
        email = UserProfileGlav.objects.get(id=self.get_object().id).email
        phone = UserProfile.objects.get(id=self.get_object().id).phone
        if not phone:
            phone = UserProfileGlav.objects.get(id=self.get_object().id).phone
        self.initial = {"email":email, 'phone':phone}
        return self.initial


    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(UsersPAdminEditView, self).dispatch(*args, **kwargs)

#Удаление пользователя
class UsersPAdminDeleteView(DeleteView):
    model = UserProfile
    template_name = 'padmin/padmin-сonfirm-delete.html'

    def get_context_data(self, **kwargs):
        context = super(UsersPAdminDeleteView, self).get_context_data(**kwargs)
        f = UserProfileGlav.objects.get(id=self.get_object().id)
        f.delete()
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_staff:
                return redirect('auth_login_padmin')
        return super(UsersPAdminDeleteView, self).dispatch(*args, **kwargs)


#Изменение цены на лид
def summa_lead(request):
    context = RequestContext(request)
    if not request.user.is_staff:
                return redirect('auth_login_padmin')
    value = UserProfile.objects.get(id=request.user.id).cont_lid
    if request.POST:
        try:
            user = UserProfile.objects.get(id=request.user.id)
            user.old_cont_lid = user.cont_lid
            user.cont_lid = request.POST['summa']
            user.save()
            usert = UserProfile.objects.get(id=1)
            usert.old_cont_lid = user.cont_lid
            usert.cont_lid = request.POST['summa']
            usert.save()
            users = UserProfileGlav.objects.get(id=request.user.id)
            users.old_cont_lid = user.cont_lid
            users.cont_lid = request.POST['summa']
            users.save()
            usersv = UserProfileGlav.objects.get(id=1)
            usersv.old_cont_lid = user.cont_lid
            usersv.cont_lid = request.POST['summa']
            usersv.save()
            context['value'] = user.cont_lid
            context['yes'] = u'Цена за лид успешно отредактированна!'
            return render_to_response('padmin/padmin-summa.html', context)
        except:
            context['value'] = value
            context['err'] = u'Ошибка! Проверьте введенные данные!'
            return render_to_response('padmin/padmin-summa.html', context)
    context['value'] = value
    return render_to_response('padmin/padmin-summa.html', context)


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login-padmin.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    # Ensure the user-originating redirection url is safe.
    print(redirect_to)
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        pass
        #redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.user.is_authenticated():
        pass

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    context = {
        'form': form
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)