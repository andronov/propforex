from django.conf.urls import patterns, url, include
from arcticles.models import Article
#from views import ArticleDetail
from django.views.generic import DetailView, RedirectView
from padmin.views import *

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='http://partner.propforex.co/admin/stat'), name='padmin-rederict'),

    url(r'^stat', StatPadminView.as_view(), name='padmin-stat'),

    url(r'^users/delete/(?P<pk>[-\w]+)/$', UsersPAdminDeleteView.as_view(success_url=reverse_lazy('padmin-users')), name='padmin-users-delete'),
    url(r'^users/(?P<pk>[-\w]+)/$', UsersPAdminEditView.as_view(success_url=reverse_lazy('padmin-users')), name='padmin-users-edit'),
    url(r'^users', UsersPAdminView.as_view(), name='padmin-users'),

    url(r'^leads/$', LeadsListPadminView.as_view(), name='padmin-leads'),

    url(r'^pages/add', PageListEditAddPadminView.as_view(), name='padmin-pages-add'),
    url(r'^pages/(?P<pk>[-\w]+)/$', PageListEditPadminView.as_view(), name='padmin-pages-edit'),
    url(r'^pages', PageListPadminView.as_view(), name='padmin-pages'),

    url(r'^summa', summa_lead, name='padmin-summa'),

    url(r'^notification/', NotificationEditPadminView.as_view(), name='padmin-notification'),

    url(r'^output/(?P<pk>[-\w]+)/$', ReferalOutEditPadminView.as_view(), name='padmin-output-edit'),
    url(r'^output/', ReferalOutPadminView.as_view(), name='padmin-output'),
    #url(r'^stat/$', StatView.as_view(), name='partner-stat'),
    url(r'^login/$', login, {}, name='auth_login_padmin')
)