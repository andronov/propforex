# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils import timezone
from django.db import models
from arcticles.models import Article


class Links(models.Model):
    landing = models.ForeignKey(Article, verbose_name=u'Лэндинг', blank=True, null=True)
    user_id = models.IntegerField(default=0, blank=True, null=True)
    name = models.CharField(verbose_name=u'Название', max_length=250)

    glav = models.BooleanField(default=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
            return self.name

    #куда ведет
    def partner_link(self):
        if self.glav:
            return settings.OSN_ULR
        else:
            return settings.OSN_ULR + self.landing.slug + '/'

    #Добавленная ссылка
    def partner_link_ref(self):
        if self.glav:
            return settings.OSN_ULR +  '?pl=' + str(self.user_id)+'&l='+str(self.id)
        else:
            return settings.OSN_ULR + self.landing.slug + '/?pl=' + str(self.user_id)+'&l=' +str(self.id)

#Лиды
"""
class Lids(models.Model):
    user_id = models.IntegerField(default=0, blank=True, null=True)
    channel = models.ForeignKey(Links, verbose_name=u'юзера канал')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
            return self.channel.name
"""