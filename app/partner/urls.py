from django.conf.urls import patterns, url, include
from arcticles.models import Article
#from views import ArticleDetail
from django.views.generic import DetailView
from partner.views import *
from django.contrib.auth import views as auth_views, logout
from accounts.forms import MyPasswordResetForm
from registration.backends.default.views import ActivationView

urlpatterns = patterns('',
    url(r'^$', HomePartnerView.as_view(), name='partner-home'),

    url(r'^stat/$', StatView.as_view(), name='partner-stat'),
    url(r'^stat/$', StatView.as_view(), name='cabinet-rules'),


    url(r'^leads/$', LidsListView.as_view(), name='partner-leads'),

    url(r'^links/$', PartnerLinksListView.as_view(), name='partner-links'),

    url(r'^promo/$', PromoPageView.as_view(), name='partner-promo'),

    url(r'^output/$', OutputView.as_view(), name='partner-output'),
    url(r'^output/(?P<id>[-\w]+)/$', bid_delete, name='partner-output-delete'),

    url(r'^info/$', InformationPageView.as_view(), name='partner-info'),

    #url(r'^testik/$', TestikPageView.as_view(), name='testik'),

    url(r'^settings/$', settingsuser, name='partner-settings'),

    #url(r'^(?P<slug>[-\w]+)/$', ArticleDetail.as_view(), name='common-article'),
    url(r'^login/$', login, {}, name='auth_login_partner'),
    url(r'^login/$', login, {}, name='auth_login'),
    url(r'^logout/$', logout_view, name='partner_logout'),


    url(r'^register/$', registration,
            name='registration_partner'),
    url(r'^register/$', registration,
            name='registration'),

    url(r'^password/change/$', auth_views.password_change,
            name='password_change'),
    url(r'^password/change/done/$', auth_views.password_change_done,
            name='password_change_done'),
    url(r'^password/reset/$', auth_views.password_reset,
            {'password_reset_form': MyPasswordResetForm}, name='password_reset'),
    url(r'^password/reset/done/$',
            auth_views.password_reset_done, name='password_reset_done'),
    url(r'^password/reset/complete/$',
            auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
            auth_views.password_reset_confirm, name='password_reset_confirm'),

    #url(r'^activate/(?P<activation_key>\w+)/$',
    #                       ActivationView.as_view(),
    #                       name='registration_activate_partner'),

    url(r'^stat/$', StatView.as_view(), name='cabinet-view'),

    (r'^accounts/', include('registration.backends.default.urls')),

    #partner admin
    url(r'^admin/', include('padmin.urls')),
    )