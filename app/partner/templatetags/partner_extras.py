# -*- coding: utf-8 -*-
from django import template
from webaccounts.models import ReferalHistory, Referal, UserProfile

register = template.Library()

#переходы
@register.filter(name='visit')
def visit(value,arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    return result.count()

#лиды
@register.filter(name='leads')
def leads(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_refer(refer_arr)
    else:
        return 0

#лиды
@register.filter(name='leadsadmin')
def leadsadmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = Referal.objects.all()[0]
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_refer_admin(refer_arr)
    else:
        return 0

#ожидающие
@register.filter(name='waits')
def waits(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_wait(refer_arr)
    else:
        return 0

#ожидающие
@register.filter(name='waitsadmin')
def waitsadmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_wait_admin(refer_arr)
    else:
        return 0

#принятые padmin
@register.filter(name='receivedadmin')
def receivedadmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_received_admin(refer_arr)
    else:
        return 0

#принятые
@register.filter(name='received')
def received(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_received(refer_arr)
    else:
        return 0

#выплаченные
@register.filter(name='outputadmin')
def outputadmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_output_admin(refer_arr)
    else:
        return 0

#выплаченные
@register.filter(name='output')
def output(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.all_output(refer_arr)
    else:
        return 0

#начисленная
@register.filter(name='partneradmin')
def partneradmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.summa_buy_partner_admin(refer_arr)
    else:
        return 0

#начисленная
@register.filter(name='partner')
def partner(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.summa_buy_partner(refer_arr)
    else:
        return 0

#начисленная
@register.filter(name='summaadmin')
def summaadmin(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.summa_buy_partner_admin_all(refer_arr)
    else:
        return 0

#epc
@register.filter(name='epc')
def epc(value, arg):
    date_ = arg
    result = value.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)
    if result:
        ref = result[0].ref
        refer_arr = []
        for re in result:
            refer_arr.append(re.id)
        return ref.epc_partner(refer_arr)
    else:
        return 0

#balance
@register.filter(name='balance')
def balance(value):
    return UserProfile.objects.get(id=value).balance()


#price lid
@register.filter(name='price')
def price(value):
    return UserProfile.objects.get(id=value).cont_lid

#full name
@register.filter(name='full_name')
def full_name(value):
    return UserProfile.objects.get(id=value).get_full_name()

@register.filter(name='price_lid')
def price_lid(value):
    return UserProfile.objects.get(id=value).cont_lid