# -*- coding: utf-8 -*-
import datetime
import hashlib
import time
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, resolve_url, render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import TemplateView, ListView
from accounts.forms import CustomAuthenticationForm
from webaccounts.models import Referal, ReferalHistory, BidPayment, UserProfile
from arcticles.models import Article
from accounts.models import UserProfile as UserProfileGlav
from partner.forms import CustomUserRegistrationFormPartner
from partner.models import Links
from datetime import datetime as dt

class HomePartnerView(TemplateView):
    template_name = 'partner/base.html'

    def get_context_data(self, **kwargs):
        context = super(HomePartnerView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(HomePartnerView, self).dispatch(*args, **kwargs)

#Информация
class InformationPageView(TemplateView):
    template_name = 'partner/partner-info.html'

    def get_context_data(self, **kwargs):
        context = super(InformationPageView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(InformationPageView, self).dispatch(*args, **kwargs)

#Статистика пользователя
class StatView(TemplateView):
    template_name = 'partner/partner-stat.html'

    def get_context_data(self, **kwargs):
        context = super(StatView, self).get_context_data(**kwargs)
        ref = Referal.objects.get(user_id=self.request.user.id)
        refer = ReferalHistory.objects.filter(ref=ref)
        context['links'] = Links.objects.filter(user_id=self.request.user.id)

        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
                refer_arr = []
                ends_d = datetime.datetime.now()
                date_dat = [ends_d]
                starts_d = end_date - relativedelta(months=1)
                while ends_d >= starts_d:
                   ends_d -= datetime.timedelta(days=1)
                   #print(ends_d - datetime.timedelta(days=1))
                   date_dat.append(ends_d)
                print(date_dat)

                for re in refer:
                    refer_arr.append(re.id)

                date_daty = []
                for dat in date_dat:
                    refers = ReferalHistory.objects.filter(ref=ref, created__day=dat.day, created__month=dat.month, created__year=dat.year)
                    if refers:
                        date_daty.append(dat)


                context['week'] = True
                context['ref_list'] = refer
                context['date_dats'] = date_daty
                context['visit'] = refer.count()
                context['leads'] = ref.all_refer(refer_arr)
                context['waits'] = ref.all_wait(refer_arr)
                context['received'] = ref.all_received(refer_arr)
                context['output'] = ref.all_output(refer_arr)
                context['output'] = ref.all_output(refer_arr)
                context['partnerka'] = ref.summa_buy_partner(refer_arr)
                context['epc'] = ref.epc_partner(refer_arr)
                return context
        elif 'time_start' in self.request.GET and 'time_finish' in self.request.GET and 'channel' in self.request.GET:
            time_start, time_finish = self.request.GET.get('time_start'), self.request.GET.get('time_finish')
            if not time_start and not time_finish:
                #time_start_dt  = datetime.datetime.now()
                time_start_dt = datetime.datetime.now() - datetime.timedelta(weeks=1)
                time_finish_dt= datetime.datetime.now()
                t = time_start_dt.strftime("%d.%m.%Y")
                time_start_dt = dt.strptime(str(t), "%d.%m.%Y")
                s = time_finish_dt.strftime("%d.%m.%Y")
                time_finish_dt = dt.strptime(str(s), "%d.%m.%Y")
                context['time_start'] = t
                context['time_finish'] = s
            elif not time_start:
                context['time_start_error'] = u'Введите дату начала сортировки'
                return context
            elif not time_finish:
                context['time_finish_error'] = u'Введите дату окончания сортировки'
                return context
            else:
                time_start_dt = dt.strptime(time_start, "%d.%m.%Y")
                time_finish_dt = dt.strptime(time_finish, "%d.%m.%Y")
                context['time_start'] = time_start
                context['time_finish'] = time_finish

            date_dat = [time_start_dt]
            while time_start_dt <= time_finish_dt:
               time_start_dt += datetime.timedelta(days=1)
               date_dat.append(time_start_dt)
            date_dat.pop()
            date_dat.reverse()

            start_date = datetime.date(time_start_dt.year, time_start_dt.month, time_start_dt.day)
            if self.request.GET.get('channel') and not time_start and not time_finish:
                link = Links.objects.get(id=self.request.GET.get('channel'))
                refer = refer.filter(created__range=(date_dat[-1],start_date),landing=link)
                context['channel'] = link
            elif self.request.GET.get('channel'):
                link = Links.objects.get(id=self.request.GET.get('channel'))
                refer = refer.filter(created__range=(date_dat[-1],start_date),landing=link)
                context['channel'] = link
            elif not time_start and not time_finish:
                refer = refer.filter(created__range=(date_dat[-1],start_date))
            else:
                refer = refer.filter(created__range=(date_dat[-1],start_date))

            refer_arr = []
            for re in refer:
                refer_arr.append(re.id)

            date_daty = []
            for dat in date_dat:
                refers = ReferalHistory.objects.filter(ref=ref, created__day=dat.day, created__month=dat.month, created__year=dat.year)
                if refers:
                   date_daty.append(dat)


            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_daty
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer(refer_arr)
            context['waits'] = ref.all_wait(refer_arr)
            context['received'] = ref.all_received(refer_arr)
            context['output'] = ref.all_output(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            return context

        elif 'time_start' in self.request.GET and 'time_finish' in self.request.GET:
            time_start, time_finish = self.request.GET.get('time_start'), self.request.GET.get('time_finish')
            if not time_start and not time_finish:
                #time_start_dt  = datetime.datetime.now()
                time_start_dt = datetime.datetime.now() - datetime.timedelta(weeks=1)
                time_finish_dt= datetime.datetime.now()
                t = time_start_dt.strftime("%d.%m.%Y")
                time_start_dt = dt.strptime(str(t), "%d.%m.%Y")
                s = time_finish_dt.strftime("%d.%m.%Y")
                time_finish_dt = dt.strptime(str(s), "%d.%m.%Y")
                context['time_start'] = t
                context['time_finish'] = s
            elif not time_start:
                context['time_start_error'] = u'Введите дату начала сортировки'
                return context
            elif not time_finish:
                context['time_finish_error'] = u'Введите дату окончания сортировки'
                return context
            else:
                time_start_dt = dt.strptime(time_start, "%d.%m.%Y")
                time_finish_dt = dt.strptime(time_finish, "%d.%m.%Y")
                context['time_start'] = time_start
                context['time_finish'] = time_finish

            date_dat = [time_start_dt]
            while time_start_dt <= time_finish_dt:
               time_start_dt += datetime.timedelta(days=1)
               date_dat.append(time_start_dt)
            date_dat.pop()
            date_dat.reverse()

            start_date = datetime.date(time_start_dt.year, time_start_dt.month, time_start_dt.day)

            if  time_start and time_finish:
                refer = refer.filter(created__range=(date_dat[-1],start_date))
            else:
                refer = refer.filter(created__range=(date_dat[-1],start_date))

            refer_arr = []
            for re in refer:
                refer_arr.append(re.id)

            date_daty = []
            for dat in date_dat:
                refers = ReferalHistory.objects.filter(ref=ref, created__day=dat.day, created__month=dat.month, created__year=dat.year)
                if refers:
                   date_daty.append(dat)


            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_daty
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer(refer_arr)
            context['waits'] = ref.all_wait(refer_arr)
            context['received'] = ref.all_received(refer_arr)
            context['output'] = ref.all_output(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            return context

        else:
            end_date = datetime.datetime.now()
            start_date = end_date - datetime.timedelta(weeks=1)
            date_ = start_date
            refer = refer.filter(created__range=(start_date, end_date))
            refer_arr = []
            ends_d = datetime.datetime.now()
            date_dat = [ends_d]
            starts_d = ends_d - datetime.timedelta(weeks=1)
            while ends_d >= starts_d:
               ends_d -= datetime.timedelta(days=1)
               #print(ends_d - datetime.timedelta(days=1))
               date_dat.append(ends_d)
               #date_dat.append(ends_d)
            print(date_dat)

            for re in refer:
                refer_arr.append(re.id)

            date_daty = []
            for dat in date_dat:
                refers = ReferalHistory.objects.filter(ref=ref, created__day=dat.day, created__month=dat.month, created__year=dat.year)
                if refers:
                   date_daty.append(dat)

            context['week'] = True
            context['ref_list'] = refer
            context['date_dats'] = date_daty
            context['visit'] = refer.count()
            context['leads'] = ref.all_refer(refer_arr)
            context['waits'] = ref.all_wait(refer_arr)
            context['received'] = ref.all_received(refer_arr)
            context['output'] = ref.all_output(refer_arr)
            context['output'] = ref.all_output(refer_arr)
            context['partnerka'] = ref.summa_buy_partner(refer_arr)
            context['epc'] = ref.epc_partner(refer_arr)
            return context



        """
        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-7':
                end_date = datetime.datetime.now()
                start_date = end_date - datetime.timedelta(weeks=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-365':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(years=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
        else:
            end_date = datetime.datetime.now()
            start_date = end_date - datetime.timedelta(weeks=1)
            date_ = start_date
            refer = refer.filter(created__range=(start_date, end_date))
            #date_ = datetime.datetime.now()
            #refer = refer.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)

        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)


        context['ref_list'] = refer
        context['ref'] = ref
        context['dates'] = date_
        context['all_refer'] = ref.all_refer(refer_arr)
        context['count_buy'] = ref.count_buy(refer_arr)
        context['summa_buy'] = ref.summa_buy(refer_arr)
        context['amount_buy_two'] = ref.amount_buy_two(refer_arr)
        context['epct'] = ref.epc(refer_arr)
        context['epc'] = ref.amount_buy(refer_arr2)
        return context
        """

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(StatView, self).dispatch(*args, **kwargs)


#Лиды
class LidsListView(ListView):
    template_name = 'partner/partner-lids-list.html'
    queryset = ReferalHistory.objects.all().order_by('-created')

    def get_queryset(self):
        qs = super(LidsListView, self).get_queryset()
        ref = Referal.objects.get(user_id=self.request.user.id)
        qs = qs.filter(refer__isnull=False, ref=ref)
        return qs

    def get_context_data(self, **kwargs):
        context = super(LidsListView, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(publish=True)
        return context


    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(LidsListView, self).dispatch(*args, **kwargs)

#Реферальные ссылки
class PartnerLinksListView(TemplateView):
    template_name = 'partner/partner-links-list.html'


    def get_context_data(self, **kwargs):
        context = super(PartnerLinksListView, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(publish=True)
        context['object_list'] = Links.objects.filter(user_id=self.request.user.id).order_by('-created')
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        name = request.POST['name']
        sistema = request.POST['sistema']
        print(name,sistema)
        if not name:
            context['errors_name'] = u'Введите название канала'
        elif sistema == '0':
            context['errors_sistema'] = u'Выберите реферальную ссылку'
        else:
            if str(sistema) == 'g':
                Links.objects.create(user_id=int(self.request.user.id), name=name, glav=True)
            else:
                art = Article.objects.get(id=int(sistema))
                Links.objects.create(landing=art, user_id=int(self.request.user.id), name=name,  glav=False)
        return self.render_to_response(context)

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(PartnerLinksListView, self).dispatch(*args, **kwargs)


#промо-материалы
class PromoPageView(TemplateView):
    template_name = 'partner/partner-promo.html'

    def get_context_data(self, **kwargs):
        context = super(PromoPageView, self).get_context_data(**kwargs)
        return context


    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(PromoPageView, self).dispatch(*args, **kwargs)

#Вывод средств
class OutputView(TemplateView):
    template_name = 'partner/partner-output.html'

    def get_context_data(self, **kwargs):
        context = super(OutputView, self).get_context_data(**kwargs)
        ref = Referal.objects.get(user_id=self.request.user.id)
        refer = ReferalHistory.objects.filter(ref=ref)
        refer_arr2 = []
        for re in refer:
            refer_arr2.append(re.id)
        if 'day' in self.request.GET:
            if str(self.request.GET.get('day')) == '-7':
                end_date = datetime.datetime.now()
                start_date = end_date - datetime.timedelta(weeks=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-30':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(months=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
            if str(self.request.GET.get('day')) == '-365':
                end_date = datetime.datetime.now()
                start_date = end_date - relativedelta(years=1)
                date_ = start_date
                refer = refer.filter(created__range=(start_date, end_date))
        else:
            date_ = datetime.datetime.now()
            refer = refer.filter(created__day=date_.day, created__month=date_.month, created__year=date_.year)

        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)



        bid = 0
        bids = BidPayment.objects.filter(user=self.request.user)
        for bd in bids:
            bid = bid + bd.sum

        context['ref_list'] = refer
        context['ref'] = ref
        context['dates'] = date_
        context['all_refer'] = ref.all_refer(refer_arr)
        context['count_buy'] = ref.count_buy(refer_arr)
        context['summa_buy'] = ref.summa_buy(refer_arr)
        context['amount_buy_two'] = ref.amount_buy_two(refer_arr)
        context['epct'] = ref.epc(refer_arr)
        context['epc'] = ref.amount_buy(refer_arr2)
        context['job_out'] = bid
        context['balance'] = ref.amount_buy(refer_arr2) - bid
        context['historys'] = BidPayment.objects.filter(user=self.request.user).order_by('-created')
        context['users'] = UserProfile.objects.get(id=self.request.user.id)
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        status = False
        sistema = False
        ref = Referal.objects.get(user_id=self.request.user.id)
        users = UserProfile.objects.get(id=self.request.user.id)
        refer = ReferalHistory.objects.filter(ref=ref)
        bid = 0
        bids = BidPayment.objects.filter(user=self.request.user)
        for bd in bids:
            bid = bid + bd.sum
            if bd.result == 1 or bd.result == 0:
               status = True
        refer_arr = []
        for re in refer:
            refer_arr.append(re.id)
        if users.sistemas == 0 or users.sistemas == 1:
            sistema = False
        else:
            sistema = True
        if 'summa' in request.POST:
          summa = request.POST.get('summa')
          try:
              int(summa)
          except:
              context['errors'] = 'Введите число'
              return self.render_to_response(context)
          if not summa:
            context['errors'] = 'Введите сумму'
          elif summa == '0':
            context['errors'] = 'Сумма не может быть равна 0'
          elif int(summa) < int(settings.MONEY_OUT):
            context['errors'] = 'Сумму на вывод должна быть от ' + str(settings.MONEY_OUT)
          elif int(summa) > int(users.balance()):
            context['errors'] = 'Ошибка! Сумма больше возможной на вывод'
          elif status:
            context['errors'] = 'У Вас уже есть созданные заявки!'
          elif sistema:
            context['errors'] = 'Сначала добавьте систему вывода!'
          else:
            try:
               BidPayment.objects.create(user=users, sum=int(summa))
               context['yesout'] = True
            except:
               context['yesout'] = False
          return self.render_to_response(context)
        if 'number' in request.POST:
            number = request.POST.get('number')
            sistema = request.POST.get('sistema')
            if not number or not sistema:
               context['errorss'] = 'Введите данные'
            else:
                users.sistemas = sistema
                users.number_sistem = number
                users.save()
                context['success'] = 'Система вывода успешно добавлена!'
            return self.render_to_response(context)

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(OutputView, self).dispatch(*args, **kwargs)


from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
            logout as auth_logout, get_user_model, logout)


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login-partner.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=CustomAuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    # Ensure the user-originating redirection url is safe.
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url('partner-stat')

    if request.user.is_authenticated():
        return redirect(resolve_url('partner-stat'))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    context = {
        'form': form
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)

def logout_view(request):
    logout(request)
    return redirect('http://propforex.co')

def _createHash():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:29]

def registration(request, code=''):
    """
    Registration view with additional field referral code and add user to group
    """
    if request.user.is_authenticated():
       if not request.user.is_webmaster:
           return redirect('http://propforex.co')
    referral_code = ''
    if not code and request.COOKIES.get('ref'):
        referral_code = request.COOKIES.get('ref')
    else:
        referral_code = code
    form = CustomUserRegistrationFormPartner(initial={'username': str(_createHash())})
    if request.method == 'POST':
        t = UserProfileGlav.objects.filter(email=request.POST['email'])
        p = UserProfile.objects.filter(email=request.POST['email'])
        if t:
                variables = RequestContext(request, {'erremail': True})
                return render_to_response('registration/registration_form_partner.html', variables)
        elif p:
                variables = RequestContext(request, {'erremail': True})
                return render_to_response('registration/registration_form_partner.html', variables)
        else:

          form = CustomUserRegistrationFormPartner(request.POST)
          print(form.errors)
          if form.is_valid():
            form.save()
            usersv = UserProfileGlav.objects.get(id=1)
            usersv.phone = request.POST['phone']
            usersv.save()
            print(form.cleaned_data['username'])
            usr = UserProfileGlav.objects.get(username=form.cleaned_data['username'])
            usr.cont_lid = usersv.cont_lid
            usr.save()
            user = UserProfile()

            user.id = usr.id
            user.phone = request.POST['phone']
            user.username = usr.username
            user.last_name = usr.last_name
            user.first_name = usr.first_name
            user.is_webmaster = True
            user.is_go = True
            user.cont_lid = usersv.cont_lid
            user.save()
            # try to find referrer
            ref = Referal.objects.create(user_id=int(user.id))
            print(ref)


            # check and update VisitirTrack
            # VisitorTrack.objects.track_registration(request)

            # send email notifications
            #from notification import models as notification
            #notification.send([user], "registration_complete", {})
            variables = RequestContext(request, {'yes': True})
            return render_to_response('registration/registration_form_partner.html', variables)
          else:
            variables = RequestContext(request, {'yes': True, 'err':form.errors})
            return render_to_response('registration/registration_form_partner.html', variables)

    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/registration_form_partner.html', variables)

#Редактирование пользователя
def settingsuser(request):
    context = RequestContext(request)

    if not request.user.is_authenticated():
        return redirect(resolve_url('partner-stat'))
    users = UserProfile.objects.get(id=request.user.id)
    context['users'] = users
    if 'notification' in request.GET:
        noti = request.GET['notification']
        if str(noti) == 'True':
            users.send = True
            users.save()
        elif str(noti) == 'False':
            users.send = False
            users.save()
            context['noti_false'] = "Оповещение о платежах приостановлено!"
        else:
            pass
    if request.POST:
        first_name = request.POST.get('first_name')
        phone = request.POST.get('phone')
        if first_name and phone:
            user = UserProfileGlav.objects.get(id=request.user.id)
            user.first_name = first_name
            user.phone = phone
            user.is_webmaster = True
            user.save()
            user = UserProfile.objects.get(id=request.user.id)
            user.first_name = first_name
            user.phone = phone
            user.save()
            return redirect(resolve_url('partner-settings'))
        else:
            context['yesout'] = False

    return render_to_response('partner/partner-user.html', context)

#Удаление заявки на вывод
def bid_delete(request, id):
    bid = BidPayment.objects.get(id=id)
    bid.delete()
    return redirect('partner-output')

#test
class TestikPageView(TemplateView):
    template_name = 'partner/testik.html'

    def get_context_data(self, **kwargs):
        context = super(TestikPageView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
            return redirect('auth_login_partner')
        else:
            if not self.request.user.is_webmaster:
                return redirect('http://propforex.co')
        return super(TestikPageView, self).dispatch(*args, **kwargs)