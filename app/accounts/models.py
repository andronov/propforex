# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import re
import urllib2
import warnings
from django.conf import settings

from django.contrib.auth.models import AbstractBaseUser,SiteProfileNotAvailable, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core.exceptions import ImproperlyConfigured
from django.core.mail import send_mail, EmailMultiAlternatives
from django.core import validators
from django.db import models
from django.db.models.signals import post_save
from django.template.loader import render_to_string
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from registration.signals import user_activated
import requests
from partner.models import Links

class Curency(models.Model):
    value = models.PositiveIntegerField(default=1, blank=True, null=True, verbose_name=u'Значение')

class UserManager(BaseUserManager):

    def _create_user(self, username,first_name,last_name, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        print(email)
        user = self.model(username=username,first_name=first_name,last_name=last_name, email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self, username,first_name=None,last_name=None, email=None, password=None, **extra_fields):
        return self._create_user(username,first_name,last_name, email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True,
                                 **extra_fields)

class AbstractUser(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    username = models.CharField(_('username'), max_length=30, unique=True,
        help_text=_('Required. 30 characters or fewer. Letters, numbers and '
                    '@/./+/-/_ characters'),
        validators=[
            validators.RegexValidator(re.compile('^[\w.@+-]+$'), _('Enter a valid username.'), 'invalid')
        ])
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    phone = models.CharField(_('Телефон'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'
    FIRST_NAME_FIELD = 'first_name'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        abstract = True

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def get_profile(self):
        """
        Returns site-specific profile for this user. Raises
        SiteProfileNotAvailable if this site does not allow profiles.
        """
        warnings.warn("The use of AUTH_PROFILE_MODULE to define user profiles has been deprecated.",
            DeprecationWarning, stacklevel=2)
        if not hasattr(self, '_profile_cache'):
            from django.conf import settings
            if not getattr(settings, 'AUTH_PROFILE_MODULE', False):
                raise SiteProfileNotAvailable(
                    'You need to set AUTH_PROFILE_MODULE in your project '
                    'settings')
            try:
                app_label, model_name = settings.AUTH_PROFILE_MODULE.split('.')
            except ValueError:
                raise SiteProfileNotAvailable(
                    'app_label and model_name should be separated by a dot in '
                    'the AUTH_PROFILE_MODULE setting')
            try:
                model = models.get_model(app_label, model_name)
                if model is None:
                    raise SiteProfileNotAvailable(
                        'Unable to load the profile model, check '
                        'AUTH_PROFILE_MODULE in your project settings')
                self._profile_cache = model._default_manager.using(
                                   self._state.db).get(user__id__exact=self.id)
                self._profile_cache.user = self
            except (ImportError, ImproperlyConfigured):
                raise SiteProfileNotAvailable
        return self._profile_cache

class UserProfile(AbstractUser):
    TYPE_ONE = 0
    TYPE_TWO = 1

    TYPE_SYSTEM = (
        (TYPE_ONE, u'QIWI'),
        (TYPE_TWO, u'Яндекс.Деньги'),
    )
    sistemas = models.IntegerField(choices=TYPE_SYSTEM, blank=True, null=True, default=0, verbose_name=u'Платежная система')
    number_sistem = models.CharField(_('Номер для вывода'), max_length=30, blank=True, null=True)
    send = models.BooleanField(default=True)
    lp = models.BooleanField(default=False)
    is_go = models.BooleanField(default=False)

    is_webmaster = models.BooleanField(default=False)
    cont_lid = models.PositiveIntegerField(default=1, verbose_name="Цена за лид")
    old_cont_lid = models.PositiveIntegerField(default=1, verbose_name="Старая цена за лид")
    buy_ticket = models.BooleanField(default=False)

    otchestvo = models.CharField(verbose_name=u'Отчество', max_length=80, blank=True)
    country = models.CharField(verbose_name=u'Страна', max_length=80, blank=True)
    city = models.CharField(verbose_name=u'Город', max_length=80, blank=True)
    birthday = models.CharField(verbose_name=u'Дата рождения',max_length=20, blank=True,null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    """
    Users within the Django authentication system are represented by this
    model.

    Username, password and email are required. Other fields are optional.
    """
    def __unicode__(self):
        if self.first_name or self.last_name:
            full_name = '%s %s' % (self.first_name, self.last_name)
            return full_name.strip()
        else:
            return self.username

    def balance(self):
        ref = Referal.objects.get(user_id=self.id)
        return ref.job_out_common_partner()

    #Подсчитываем количество тикетов
    def count_ticket(self):
        return self.user_ticket.count()

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

class Referal(models.Model):
    user_id = models.IntegerField(default=0, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    #Баланс     parnter
    def job_out(self, arg):
        if self.amount_buy_partner() == 0:
            return 0
        if arg == 0:
            return self.epc_partner()
        elif arg:
            sum = self.epc_partner() - arg
            return sum

    #Баланс общий part
    def job_out_common_partner(self):
        return self.summa_buy_partner() - self.out()

    #сумма купленных билетов партнерка
    def amount_buy_partner(self, arg=None):
        if arg:
            return self.summa_buy_partner(arg)*0.1
        elif arg == []:
            return 0
        else:
            return self.summa_buy_partner()*0.1

    #сумма купленных билетов EPC партнерка
    def epc_partner(self, arg=None):
        if self.amount_buy_partner() == 0:
            return 0
        elif arg:
            return round(self.amount_buy_partner(arg)/self.all_transition(arg), 2)
        elif arg == []:
            return 0
        else:
            return round(self.amount_buy_partner()/self.all_transition(), 2)

    #сумма оплачиваемых билетов партнерка padmin
    def summa_buy_partner_admin_all(self, arg=None):
        refer = 0
        if arg:
             for s in ReferalHistory.objects.filter(id__in=arg):
                  if s.refer:
                      p = s.refer.user_ticket.all().order_by('created')
                      if s.result == 2:
                          if p:
                              refer = refer + s.price_lid
                          else:
                              refer = 0

        elif arg == []:
            return 0
        else:
             for s in ReferalHistory.objects.filter(id__in=arg):
                  if s.refer:
                      p = s.refer.user_ticket.all().order_by('created')
                      if s.result == 2:
                          if p:
                              refer = refer + s.price_lid
                          else:
                              refer = 0
        return refer

    #сумма all tickets padmin
    def summa_buy_partner_admin(self, arg=None):
        refer = 0
        if arg:
             for s in ReferalHistory.objects.filter(id__in=arg):
                  if s.refer:
                     for d in s.refer.user_ticket.all():
                        refer = refer + d.all_amounts()
        elif arg == []:
            return 0
        else:
             for s in ReferalHistory.objects.filter(id__in=arg):
                  if s.refer:
                     for d in s.refer.user_ticket.all():
                        refer = refer + d.all_amounts()
        return refer

    #сумма оплачиваемых билетов партнерка
    def summa_buy_partner(self, arg=None):
        refer = 0
        if arg:
             for s in self.ref_history.filter(id__in=arg):
                  if s.refer:
                      p = s.refer.user_ticket.all().order_by('-created')
                      if s.result == 2:
                          if p:
                              refer = refer + s.price_lid
                          else:
                              refer = 0

        elif arg == []:
            return 0
        else:
             for s in self.ref_history.all():
                  if s.refer:
                      p = s.refer.user_ticket.all().order_by('-created')
                      if s.result == 2:
                          if p:
                              refer = refer + s.price_lid
                          else:
                              refer = 0
        return refer

    #количество переходов
    def all_transition(self, arg=None):
        if arg:
            return self.ref_history.filter(id__in=arg).count()
        else:
            return self.ref_history.count()

    #количество регистраций padmin
    def all_refer_admin(self, arg=None):
        refer = []
        if arg:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.refer:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in ReferalHistory.objects.all():
                if s.refer:
                    refer.append(s.refer.id)
        return len(refer)

    #количество регистраций
    def all_refer(self, arg=None):
        refer = []
        if arg:
            for s in self.ref_history.filter(id__in=arg):
                if s.refer:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in self.ref_history.all():
                if s.refer:
                    refer.append(s.refer.id)
        return len(refer)
        refer = []

    #количество ожидающих билетов padmin
    def all_wait_admin(self, arg=None):
        refer = []
        if arg:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 1:
                    if s.refer:
                        refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 1:
                    if s.refer:
                        refer.append(s.refer.id)
        return len(refer)

    #количество ожидающих билетов
    def all_wait(self, arg=None):
        refer = []
        if arg:
            for s in self.ref_history.filter(id__in=arg):
                if s.result == 1:
                    if s.refer:
                        refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in self.ref_history.all():
                if s.result == 1:
                    if s.refer:
                        refer.append(s.refer.id)
        return len(refer)

    #количество принятых билетов
    def all_received_admin(self, arg=None):
        refer = []
        if arg:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 2:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 2:
                    refer.append(s.refer.id)
        return len(refer)

    #количество принятых билетов
    def all_received(self, arg=None):
        refer = []
        if arg:
            for s in self.ref_history.filter(id__in=arg):
                if s.result == 2:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in self.ref_history.all():
                if s.result == 2:
                    refer.append(s.refer.id)
        return len(refer)

    #количество выплаченных билетов padmin
    def all_output_admin(self, arg=None):
        refer = []
        if arg:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 3:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in ReferalHistory.objects.filter(id__in=arg):
                if s.result == 3:
                    refer.append(s.refer.id)
        return len(refer)

    #количество выплаченных билетов
    def all_output(self, arg=None):
        refer = []
        if arg:
            for s in self.ref_history.filter(id__in=arg):
                if s.result == 3:
                    refer.append(s.refer.id)
        elif arg == []:
            return 0
        else:
            for s in self.ref_history.all():
                if s.result == 3:
                    refer.append(s.refer.id)
        return len(refer)

    #количество оплачиваемых билетов
    def count_buy(self, arg=None):
        refer = 0
        if arg:
             for s in self.ref_history.filter(id__in=arg):
                 if s.refer:
                     refer = refer + s.refer.user_ticket.all().count()
        elif arg == []:
            return 0
        else:
             for s in self.ref_history.all():
                 if s.refer:
                     refer = refer + s.refer.user_ticket.all().count()
        return refer

    #сумма оплачиваемых билетов
    def summa_buy(self, arg=None):
        refer = 0
        if arg:
             for s in self.ref_history.filter(id__in=arg):
                  if s.refer:
                     for d in s.refer.user_ticket.all():
                        refer = refer + d.all_amounts()
        elif arg == []:
            return 0
        else:
             for s in self.ref_history.all():
                  if s.refer:
                     for d in s.refer.user_ticket.all():
                        refer = refer + d.all_amounts()
        return refer

    #начисленная
    def amount_buy(self, arg=None):
        if arg:
            return self.summa_buy(arg)*0.1
        else:
            return self.summa_buy()*0.1

    #начисленная
    def amount_buy_two(self, arg=None):
        if arg:
            return self.summa_buy(arg)*0.1
        elif arg == []:
            return 0
        else:
            return self.summa_buy()*0.1

    #epc
    def epc(self, arg=None):
        if self.amount_buy() == 0:
            return 0
        elif arg:
            return round(self.amount_buy(arg)/self.all_transition(arg), 2)
        elif arg == []:
            return 0
        else:
            return round(self.amount_buy()/self.all_transition(), 2)

    #Баланс
    def job_out(self, arg):
        if self.amount_buy() == 0:
            return 0
        if arg == 0:
            return self.epc()
        elif arg:
            sum = self.epc() - arg
            return sum

    #Баланс общий
    def job_out_common(self):
        return self.amount_buy_two() - self.out() 

    def out(self):
        bid = 0
        bids = BidPayment.objects.filter(user__id=self.user_id)
        for bd in bids:
            bid = bid + bd.sum     
        return bid    




class ReferalHistory(models.Model):
    TYPE_WHO = 1
    TYPE_REP = 2
    TYPE_LIST = 3

    TYPE_CHOICES = (
        (TYPE_WHO, 'Ожидает'),
        (TYPE_REP, 'Принят'),
        (TYPE_LIST, 'Отклонен'),
    )
    ref = models.ForeignKey(Referal, related_name='ref_history')
    refer = models.ForeignKey(UserProfile, related_name='ref_users', blank=True, null="True")
    transition = models.PositiveIntegerField(default=1)

    result = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True, default=1, verbose_name=u'Статус')
    landing = models.ForeignKey(Links, verbose_name="Имя канала", blank=True, null="True")

    price_lid = models.PositiveIntegerField(default=10)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)



#Заявка на вывод средств
class BidPayment(models.Model):
    TYPE_WHO = 0
    TYPE_REP = 1
    TYPE_LIST = 2

    TYPE_CHOICES = (
        (TYPE_WHO, 'Ожидает'),
        (TYPE_REP, 'В процессе'),
        (TYPE_LIST, 'Выплачено'),
    )
    is_webmaster = models.BooleanField(default=False)
    user = models.ForeignKey(UserProfile, related_name='user_bid', verbose_name=u'Пользователь')
    result = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True, default=0, verbose_name=u'Статус')
    sum = models.PositiveIntegerField(default=0, verbose_name=u'Сумма')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

class Notification(models.Model):
    is_webmaster = models.BooleanField(default=False)
    site = models.BooleanField(default=True, blank=True, verbose_name=u'Оповещать на сайте')
    pochta = models.BooleanField(default=False, verbose_name=u'Оповещать на почту')
    email = models.EmailField(blank=True, null=True, verbose_name=u'Электронный адрес')

def send_actived(sender, user, request, **kwargs):
    users = user
    if user.is_webmaster:
       text_content = 'Информация о проекте PropForex'
       #users = request.user
       html_content = render_to_string('mailchimp/webmaster.html', {'users': users})
       msg = EmailMultiAlternatives('Информация о проекте PropForex', text_content, settings.EMAIL_HOST_USER,  [users.email])
       msg.attach_alternative(html_content, "text/html")
       msg.send()
    elif not user.lp and not user.is_webmaster:
       text_content = 'Информация о проекте PropForex'
       #users = request.user
       html_content = render_to_string('mailchimp/activate.html', {'users': users})
       msg = EmailMultiAlternatives('Информация о проекте PropForex', text_content, settings.EMAIL_HOST_USER,  [users.email])
       msg.attach_alternative(html_content, "text/html")
       msg.send()
    else:
       pass   

user_activated.connect(send_actived)



import hashlib
import time
def _createHashPas():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:7]

#Отрпавка клиента  с лэдинга
def send_actived_lp(sender, user, request, **kwargs):
    users = user
    if user.lp:
        userr = UserProfile.objects.get(id=users.id)
        past = _createHashPas()
        userr.set_password(_createHashPas())
        userr.save()
        text_content = 'Информация о проекте PropForex'
        #users = request.user
        html_content = render_to_string('mailchimp/firstmail_lp.html', {'users': users, 'past' : past})
        msg = EmailMultiAlternatives(text_content, text_content, settings.EMAIL_HOST_USER,  [users.email])
        msg.attach_alternative(html_content, "text/html")
        msg.send()
    else:
        pass   

user_activated.connect(send_actived_lp)


#Отправка оповещение после добавления вывода средств
def payment_stock(sender, instance, **kwargs):
    if kwargs["created"]:
        noti = Notification.objects.get(id=1)
        if noti.pochta:
           users = UserProfile.objects.get(username='admin')
           text_content = 'Пользователь заказал вывод средств'
           html_content = render_to_string('mailchimp/order.html', {'users': users})
           msg = EmailMultiAlternatives('Пользователь заказал вывод средств', text_content, settings.EMAIL_HOST_USER,  [noti.email])
           msg.attach_alternative(html_content, "text/html")
           msg.send()

# register the signal
post_save.connect(payment_stock, sender=BidPayment, dispatch_uid="payment_stock_count")


#Отрправка данные в лист вебмастера
def send_actived_weblist(sender, user, request, **kwargs):
    userr = UserProfile.objects.get(id=user.id)
    if userr.is_webmaster:
           name = ''
           if userr.first_name:
               name = userr.first_name
           payload = {'name': name, 'email': userr.email, 'list': settings.PARTNER_LIST}
           r = requests.post("http://mail.propforex.co/subscribe", data=payload)
           print(r)

user_activated.connect(send_actived_weblist)
