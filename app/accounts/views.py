from django.conf import settings
from django.contrib.sites.models import get_current_site
from django.http import HttpResponseRedirect
from django.shortcuts import resolve_url, redirect, render_to_response
from django.template import RequestContext
from django.template.response import TemplateResponse
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
            logout as auth_logout, get_user_model)
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
import requests
from accounts import UserProfile
from accounts.forms import CustomUserRegistrationForm, _createHash, CustomAuthenticationForm
from accounts.models import Referal
from accounts.models import ReferalHistory
from partner.models import Links


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=CustomAuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    # Ensure the user-originating redirection url is safe.
    if not is_safe_url(url=redirect_to, host=request.get_host()):
        redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

    if request.user.is_authenticated():
        return redirect(resolve_url(settings.LOGIN_REDIRECT_URL))

    if request.method == "POST":
        form = authentication_form(request, data=request.POST)
        if form.is_valid():
            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    context = {
        'form': form
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def registration(request, code=''):
    """
    Registration view with additional field referral code and add user to group
    """
    referral_code = ''
    if not code and request.COOKIES.get('ref'):
        referral_code = request.COOKIES.get('ref')
    else:
        referral_code = code
    form = CustomUserRegistrationForm(initial={'username': str(_createHash())})
    if request.method == 'POST':
        form = CustomUserRegistrationForm(request.POST)
        print(form.errors)
        if form.is_valid():
            form.save()
            print(form.cleaned_data['username'])
            user = UserProfile.objects.get(username=form.cleaned_data['username'])
            print(user)
            user.phone = form.cleaned_data['phone']
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            send_list_user(user.id)
            # try to find referrer
            ref = Referal.objects.create(user_id=int(user.id))
            print(ref)
            if 'ref' in request.COOKIES:
                if 'link' in request.COOKIES:
                     users = UserProfile.objects.get(id=int(request.COOKIES['ref']))
                     ref = Referal.objects.get(user_id=int(request.COOKIES['ref']))
                     #try:
                     link = Links.objects.get(user_id=int(request.COOKIES['ref']),landing__id=int(request.COOKIES['l']),landing__publish=True).order_by('created')
                     ReferalHistory.objects.create(ref=ref,landing=link, price_lid=users.cont_lid)
                else:
                     #refffer = Referal.objects.get(user_id=int(request.COOKIES['ref']))
                     #refer = ReferalHistory.objects.create(ref=refffer, refer=user)
                     """

                     print(refer)
                     """

            # check and update VisitirTrack
            # VisitorTrack.objects.track_registration(request)
            
            # send email notifications
            #from notification import models as notification
            #notification.send([user], "registration_complete", {})
            variables = RequestContext(request, {'yes': True})
            return render_to_response('registration/registration_form.html', variables)

    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/registration_form.html', variables)

def send_list_user(id):
    try:
           userr = UserProfile.objects.get(id=id)
           name = ''
           if userr.first_name:
               name = userr.get_full_name()
           payload = {'name': name, 'email': userr.email, 'list': settings.ALLUSERS_LIST}
           r = requests.post("http://mail.propforex.co/subscribe", data=payload)
           print(r)
    except:
        pass