from django.conf.urls import patterns, include, url
from django.contrib import admin
from accounts.forms import MyPasswordResetForm
from admin.views import UsersAdminView
import core
from core.views import HomePageView, CabinetRulesView, BuyTicketView, ActiveTicketView, InActiveTicketView, AffilateView, \
    CabinetView, MaterialView, HomePageEnView
from django.contrib.staticfiles import views
from django.contrib.auth import views as auth_views, logout
from accounts.views import (login)

admin.autodiscover()



urlpatterns = patterns('',
    url(r'^yandex_67fb7d7c49fd886a.txt', core.views.sert, name='sert'), 
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^en$', HomePageEnView.as_view(), name='home-en'),
    url(r'^cabinet/rules$', CabinetRulesView.as_view(), name='cabinet-rules'),
    url(r'^cabinet/buy-ticket/', BuyTicketView.as_view(), name='buy-ticket'),
    url(r'^cabinet/tickets/', ActiveTicketView.as_view(), name='tickets'),
    url(r'^cabinet/intickets/', InActiveTicketView.as_view(), name='intickets'),
    url(r'^cabinet/affilate/(?P<id>[-\w]+)/$', core.views.bid_delete, name='affilate-delete'),
    url(r'^cabinet/affilate/', AffilateView.as_view(), name='affilate'),
    url(r'^cabinet/material/', MaterialView.as_view(), name='material'),
    url(r'^cabinet/user/(?P<email>\w+)/',core.views.cancel_send,name="cabinet-cancel-send"),
    url(r'^cabinet/user/', core.views.edituser, name='user-edit'),

    url(r'^cabinet/', CabinetView.as_view(), name='cabinet-view'),
    url(r'^order/', core.views.order_call, name='order-call'),

    url(r'^next/', core.views.next, name='next_registration'),


    url(r'^login/$', login, {}, name='auth_login'),
    url(r'^accounts/logout/$', core.views.logout_view, name='auth_logout'),

    url(r'^accounts/register/$', 'accounts.views.registration',
            name='registration'),
    url(r'^accounts/password/change/$', auth_views.password_change,
            name='password_change'),
    url(r'^accounts/password/change/done/$', auth_views.password_change_done,
            name='password_change_done'),
    url(r'^accounts/password/reset/$', auth_views.password_reset,
            {'password_reset_form': MyPasswordResetForm}, name='password_reset'),
    url(r'^accounts/password/reset/done/$',
            auth_views.password_reset_done, name='password_reset_done'),
    url(r'^accounts/password/reset/complete/$',
            auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^accounts/password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
            auth_views.password_reset_confirm, name='password_reset_confirm'),

    (r'^accounts/', include('registration.backends.default.urls')),

    #add new user landing N1
    url(r'^new-user/lp/', core.views.new_landing_user, name='new_landing_user'),

    #urls robokassa
    url(r'^robokassa/', include('robokassa.urls')),
    url(r'^oplata/', include('robokassa.urls')),
    url(r'^oplata/(?P<ticket_id>[-\w]+)/$', core.views.pay_with_robokassa, name='cabinet-oplata'),

    #partner
    #url(r'^partner/', include('partner.urls')),

    #admin
    url(r'^manager/', include('admin.urls')),
    #url(r'^manager/', UsersAdminView.as_view(), name='admin-users'),

    url(r'^admin/', include(admin.site.urls)),

    url('', include('arcticles.urls')),
)

urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
]