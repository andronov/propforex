# -*- coding: utf-8 -*-
"""
Django settings for propforex project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.append( os.path.join(BASE_DIR, 'app') )

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&*_ljnwl=#3!e3l@#9m8zwr8*%#^r&44ax*#qqcpa$cm_@i%f$'

AUTH_USER_MODEL = 'accounts.UserProfile'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'propforex.urls'

SITE_ID = 1

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_hosts',
    'webaccounts',
    'robokassa',
    'south',
    'registration',
    'bootstrap3',
    'accounts',
    'core',
    'crispy_forms',
    'arcticles',
    'partner',
    'padmin'
)

MIDDLEWARE_CLASSES = (
    'django_hosts.middleware.HostsRequestMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_hosts.middleware.HostsRequestMiddleware',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
)

PARENT_HOST = u'www.propforex.co'
MAIN_HOST = 'http://www.propforex.co'

DEFAULT_HOST='www'

ROOT_HOSTCONF = 'propforex.hosts'

WSGI_APPLICATION = 'propforex.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'propforex',
        'USER': 'root',
        #'PASSWORD': 'rjierb18',
        'PASSWORD': 'xswnudtfmvjw1',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Dublin'

USE_I18N = True

USE_L10N = True

USE_TZ = False



# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(ROOT_PATH, 'static')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

LOGIN_URL = 'auth_login'
LOGIN_REDIRECT_URL = 'home'
ACCOUNT_ACTIVATION_DAYS = 2
LOGOUT_REDIRECT_URL = 'home'

AUTH_USER_EMAIL_UNIQUE = True
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_HOST_USER = 'invest@propforex.co' #'noreply@clovrr.com'
EMAIL_HOST_PASSWORD = '3NAuV3iAdPJ4B_ARZzvTLw'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

CRISPY_TEMPLATE_PACK = 'bootstrap3'

#Минимальная сумма на вывод
MONEY_OUT = 1000

ROBOKASSA_LOGIN = 'propforex'
ROBOKASSA_PASSWORD1 = 'ifreework71'
ROBOKASSA_PASSWORD2 = 'ifreework9'

OSN_ULR = 'http://propforex.co/'

#sandy Partners CPA
PARTNER_LIST = 'h892xsDGr8WO2d7uJ00nB0KQ'
BUYTICKET_LIST = 'tEmapNLvfZ0VGYIguAi9JQ'
ALLUSERS_LIST = 's0eMAx6cSIuUDt4NJwsVmQ'
FULLUSERS_LIST = '89UIP3590892e8BsWlubBNIQ'