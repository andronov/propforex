# -*- coding: utf-8 -*-
from django.conf import settings
from django_hosts import patterns, host


host_patterns = patterns('',
    host(r'partner', 'partner.urls', callback='partner.callbacks.host_partner', name='partner'),
    host(r'', settings.ROOT_URLCONF, name='www'),
    #host(r'(www\.)*(?P<subdomain>[0-9a-z\-]+)', 'base.urls',  callback='base.callbacks.host_school', name='subdomain-area'),
)